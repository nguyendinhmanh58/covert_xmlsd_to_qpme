/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package readxmlpapyrus;

import java.awt.Color;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import static readxmlpapyrus.SyncReadPapyrus.ColorToHex;

/**
 *
 * @author manhnd
 */
public class AltReadPapyrus {

    /**
     * @param args the command line arguments
     */
    public ArrayList<Color> colors = new ArrayList<Color>();
    public ArrayList<PapyrusLifelineTag> p_lifeline_tags = new ArrayList<PapyrusLifelineTag>();
    public ArrayList<PapyrusMessageTag> p_message_tags = new ArrayList<PapyrusMessageTag>();
    public ArrayList<QpmeCologTag> color_tags = new ArrayList<QpmeCologTag>();
    public ArrayList<QueuePlace> queue_places = new ArrayList<QueuePlace>(); // tuong ung places trong qpme
    public ArrayList<QpmeQueueTag> queue_tags = new ArrayList<QpmeQueueTag>(); // tuong ung queues trong qpmesss
    public ArrayList<PapyrusCombinedFragment> alt_fragments = new ArrayList<>();
    public ArrayList<SynchOrdinaryPlace> sync_ordinary_places = new ArrayList<SynchOrdinaryPlace>();
    public int id = 0;

    public static void main(String[] args) {
        // TODO code application logic here
        AltReadPapyrus f = new AltReadPapyrus();
        try {
            f.parseNow();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void parseNow() throws ParserConfigurationException, SAXException,
            IOException {
        // Tạo một đối tượng DocumentBuilderFactory từ method tĩnh của nó
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

        // Sét đặt việc kiểm tra tính hợp lệ của tài liệu sẽ phân tích sau này.
        factory.setValidating(true);

        // Tạo đối tượng DocumentBuilder
        DocumentBuilder builder = factory.newDocumentBuilder();

        // Lay ra nut Document (mo ta toan bo tai lieu xml cua file xmlFile.xml
        Document xmlDoc = builder.parse("E:\\GR\\UML\\ReadXmlPapyrus\\src\\readxmlpapyrus\\alt2.uml");
        // Tu nut Document xmlDoc co the lay ra phan tu goc cua tai lieu XML
        Element root = (Element) xmlDoc.getDocumentElement();
        // In ra man hinh ten cua phan tu goc cua tai lieu XML xmlFile.xml la gi
        //System.out.println("Root name: " + root.getNodeName());
        MyNodeLibrary.init();
        processNode(root);
        for (int i = 0; i < alt_fragments.size(); i++) {
            for (int j = 0; j < alt_fragments.get(i).covered_lifelines.size(); j++) {
                System.out.println("ALTTTTTTTT_LIFELINE: " + alt_fragments.get(i).covered_lifelines.get(j).name);
            }
            for (int j = 0; j < alt_fragments.get(i).operands.size(); j++) {
                System.out.println("OPERAND: " + j);
                for (int k = 0; k < alt_fragments.get(i).operands.get(j).queues.size(); k++) {
                    System.out.println("-->QUEEEEEEEEE: " + alt_fragments.get(i).operands.get(j).queues.get(k).name);
                }
            }
        }
        writeDomQPME();
    }

    private void processNode(Element element) {
        /**
         * In ra ten cua nut bang myNode.getNodeName() .Voi element ta co the
         * thay boi myElement.getTagName() cung co duoc ket qua tuong tu .Trong
         * tinh huong muon dieu khien viec in ra cac tiep dau ngu (prefix) nen
         * su dung cac method cung cap boi interface Element.
         */
        //print("<" + element.getNodeName());
        // Lay ra danh sach cac thuoc tinh cua Element element

        QueuePlace new_QueuePlace;
        QpmeQueueTag new_Queue;
        switch (MyNodeLibrary.getTagCode(element.getNodeName())) {
            case LIFELINE:
                PapyrusLifelineTag new_lifeline = new PapyrusLifelineTag();
                NamedNodeMap nodeMap = element.getAttributes();
                for (int i = 0; i < nodeMap.getLength(); i++) {
                    // Lay ra nut thu i trong tap hop
                    Attr at = (Attr) nodeMap.item(i);
                    //print(" " + at.getNodeName() + "=\"" + at.getNodeValue() + "\"");
                    switch (MyNodeLibrary.getAttributeCode(at.getNodeName())) {
                        case ID:
                            new_lifeline.id = at.getNodeValue();
                            break;
                        case NAME:
                            new_lifeline.name = at.getNodeValue();
                            break;
                        case TYPE:
                            new_lifeline.type = at.getNodeValue();
                            break;
                        case COVEREDBY:
                            new_lifeline.coveredBy = at.getNodeValue();
                            break;
                    }
                }
                new_lifeline.color = addColor();
                p_lifeline_tags.add(new_lifeline);

                // add color tag
                QpmeCologTag new_color = new QpmeCologTag();
                new_color.real_color = new_lifeline.color;
                new_color.id = new_lifeline.id;
                new_color.name = new_lifeline.name.toLowerCase();
                color_tags.add(new_color);

                // add new "queue"
                new_Queue = new QpmeQueueTag();
                new_Queue.id = getId();
                new_Queue.name = new_lifeline.name;
                queue_tags.add(new_Queue);

                new_lifeline.queue_id = new_Queue.id;

                // add new "queue place" 
                new_QueuePlace = new QueuePlace();
                new_QueuePlace.id = getId();
                new_QueuePlace.name = new_lifeline.name;
                new_QueuePlace.lifeline_id = new_lifeline.id;
                new_QueuePlace.queue_ref = new_Queue.id;
                new_lifeline.queues.add(new_QueuePlace);
                queue_places.add(new_QueuePlace);
                break;
            case MESSAGE:
                PapyrusMessageTag new_MessageTag = new PapyrusMessageTag();
                nodeMap = element.getAttributes();
                for (int i = 0; i < nodeMap.getLength(); i++) {
                    // Lay ra nut thu i trong tap hop
                    Attr at = (Attr) nodeMap.item(i);
                    //print(" " + at.getNodeName() + "=\"" + at.getNodeValue() + "\"");
                    switch (MyNodeLibrary.getAttributeCode(at.getNodeName())) {
                        case ID:
                            new_MessageTag.id = at.getNodeValue();
                            break;
                        case NAME:
                            new_MessageTag.name = at.getNodeValue();
                            break;
                        case TYPE:
                            new_MessageTag.type = at.getNodeValue();
                            break;
                        case RECEICVE_EVENT:
                            new_MessageTag.receiveEvent = at.getNodeValue();
                            break;
                        case SEND_EVENT:
                            new_MessageTag.sendEvent = at.getNodeValue();
                            break;
                        case MESSAGE_SORT:
                            new_MessageTag.messageSort = at.getNodeValue();
                            break;
                    }
                }
                new_MessageTag.color = addColor();
                p_message_tags.add(new_MessageTag);

                // add color tag
                new_color = new QpmeCologTag();
                new_color.real_color = new_MessageTag.color;
                new_color.id = new_MessageTag.id;
                new_color.name = new_MessageTag.name.toLowerCase();
                color_tags.add(new_color);
                break;
            case FRAGMENT:
                switch (MyNodeLibrary.getFragmentTypeCode(element.getAttribute("xmi:type"))) {
                    case ExecutionOccurrenceSpecification: // dai dien cho 1 excution occurece ==> tuong ung voi 1 queue
                        new_QueuePlace = new QueuePlace();
                        new_QueuePlace.id = element.getAttribute("xmi:id");
                        String covered = element.getAttribute("covered");
                        int index;
                        if ((index = getLifelineById(covered)) != -1) {
                            // tim thay lifeline covered
                            new_QueuePlace.name = p_lifeline_tags.get(index).name + p_lifeline_tags.get(index).index_ocurrence;
                            new_QueuePlace.lifeline_id = p_lifeline_tags.get(index).id;
                            new_QueuePlace.queue_ref = p_lifeline_tags.get(index).queue_id;
                            p_lifeline_tags.get(index).queues.add(new_QueuePlace);
                            p_lifeline_tags.get(index).index_ocurrence++;
                        }

                        queue_places.add(new_QueuePlace);

                        Node parent_node = element.getParentNode();
                        if (parent_node.getNodeName().equals("operand")) {
                            Element operand = (Element) parent_node;
                            Element fragment = (Element) operand.getParentNode();
                            switch (MyNodeLibrary.getInteractionOperatorCode(fragment.getAttribute("interactionOperator"))) {
                                case ALT:
                                    PapyrusCombinedFragment alt = getAltFragmentById(fragment.getAttribute("xmi:id"));
                                    alt.getOperandById(operand.getAttribute("xmi:id")).queues.add(new_QueuePlace);
                                    break;
                                default:
                                    break;
                            }
                        }
                        break;
                    case ActionExecutionSpecification: // dai dien cho 1 excution
                        break;
                    case MessageOccurrenceSpecification: // dai dienj cho 1 message occurrence ==> tuong ung 1 queue
                        new_QueuePlace = new QueuePlace();
                        new_QueuePlace.id = element.getAttribute("xmi:id");
                        covered = element.getAttribute("covered");
                        if ((index = getLifelineById(covered)) != -1) {
                            // tim thay lifeline covered
                            new_QueuePlace.name = p_lifeline_tags.get(index).name + p_lifeline_tags.get(index).index_ocurrence;
                            new_QueuePlace.lifeline_id = p_lifeline_tags.get(index).id;
                            new_QueuePlace.queue_ref = p_lifeline_tags.get(index).queue_id;
                            p_lifeline_tags.get(index).queues.add(new_QueuePlace);
                            p_lifeline_tags.get(index).index_ocurrence++;
                        }
                        queue_places.add(new_QueuePlace);

                        parent_node = element.getParentNode();
                        if (parent_node.getNodeName().equals("operand")) {
                            Element operand = (Element) parent_node;
                            Element fragment = (Element) operand.getParentNode();
                            switch (MyNodeLibrary.getInteractionOperatorCode(fragment.getAttribute("interactionOperator"))) {
                                case ALT:
                                    PapyrusCombinedFragment alt = getAltFragmentById(fragment.getAttribute("xmi:id"));
                                    alt.getOperandById(operand.getAttribute("xmi:id")).queues.add(new_QueuePlace);
                                    break;
                                default:
                                    break;
                            }
                        }

                        break;
                    case CombinedFragment:
                        PapyrusCombinedFragment new_fragment = new PapyrusCombinedFragment();
                        nodeMap = element.getAttributes();
                        for (int i = 0; i < nodeMap.getLength(); i++) {
                            // Lay ra nut thu i trong tap hop
                            Attr at = (Attr) nodeMap.item(i);
                            //print(" " + at.getNodeName() + "=\"" + at.getNodeValue() + "\"");
                            switch (MyNodeLibrary.getAttributeCode(at.getNodeName())) {
                                case ID:
                                    new_fragment.id = at.getNodeValue();
                                    break;
                                case NAME:
                                    new_fragment.name = at.getNodeValue();
                                    break;
                                case TYPE:
                                    new_fragment.type = at.getNodeValue();
                                    break;
                                case COVERED:
                                    new_fragment.covered_lifelines = getCoveredLifeline(at.getNodeValue());
                            }
                        }

                        switch (MyNodeLibrary.getInteractionOperatorCode(element.getAttribute("interactionOperator"))) {
                            case ALT:
                                alt_fragments.add(new_fragment);
                                NodeList operands = element.getElementsByTagName("operand");
                                for (int operand_index = 0; operand_index < operands.getLength(); operand_index++) {
                                    Element operand = (Element) operands.item(operand_index);
                                    PapyrusOperandTag new_operand_tag = new PapyrusOperandTag();
                                    NamedNodeMap node_operand_map = operand.getAttributes();
                                    for (int j = 0; j < node_operand_map.getLength(); j++) {
                                        // Lay ra nut thu i trong tap hop
                                        Attr at = (Attr) node_operand_map.item(j);
                                        //print(" " + at.getNodeName() + "=\"" + at.getNodeValue() + "\"");
                                        switch (MyNodeLibrary.getAttributeCode(at.getNodeName())) {
                                            case ID:
                                                new_operand_tag.id = at.getNodeValue();
                                                break;
                                            case NAME:
                                                new_operand_tag.name = at.getNodeValue();
                                                break;
                                            case TYPE:
                                                new_operand_tag.type = at.getNodeValue();
                                                break;
                                            case COVERED:
                                                new_operand_tag.covered_lifelines = getCoveredLifeline(at.getNodeValue());
                                                break;
                                        }
                                    }
                                    new_fragment.operands.add(new_operand_tag);
                                }
                                break;
                            default:
                                break;
                        }
                        break;

                    default:
                        break;
                }

                break;
        }
        // print(">");
        // Lay ra danh sach cac nut con cua nut Element element
        NodeList nodeList = element.getChildNodes();
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node node = nodeList.item(i);
            short type = node.getNodeType();
            switch (type) {
                // Truong hop nut con thu i la mot Element
                case Node.ELEMENT_NODE:
                    processNode((Element) node);// goi method nay cho Element
                    break;
                case Node.COMMENT_NODE:
                    //print("<!-- " + node.getNodeValue() + " -->");
                    break;
                default:
                    /**
                     * Trong cac truong hop nut con la mot kieu nut khac .... De
                     * don gian trong vi du nay khong demo cac truong hop do
                     * .Tuy nhien se co mot vi du demo ve viec su ly toan bo cac
                     * kieu nut trong tai lieu xml .
                     */
                    // print("Other node.....");
                    break;
            }
        }
    }

    public ArrayList<PapyrusLifelineTag> getCoveredLifeline(String covered) {
        ArrayList<PapyrusLifelineTag> covered_lifelines = new ArrayList<PapyrusLifelineTag>();
        String[] arr_covered = covered.split(" ");
        for (int i = 0; i < arr_covered.length; i++) {
            for (Iterator<PapyrusLifelineTag> iterator = p_lifeline_tags.iterator(); iterator.hasNext();) {
                PapyrusLifelineTag lifelineTag = iterator.next();
                if (lifelineTag.id.equals(arr_covered[i])) {
                    covered_lifelines.add(lifelineTag);
                    break;
                }
            }
        }
        return covered_lifelines;
    }

    /**
     * Su dung trong method processNode(...)
     */
    private void print(String s) {
        System.out.println(s);
    }

    public String addColor() {
        Color new_color = randomColor();
        while (colors.contains(new_color) == true) {
            new_color = randomColor();
        }
        colors.add(new_color);
        return ColorToHex(new_color);
    }

    public Color randomColor() {
        Random random = new Random(); // Probably really put this somewhere where it gets executed only once
        int red = random.nextInt(256);
        int green = random.nextInt(256);
        int blue = random.nextInt(256);
        return new Color(red, green, blue);
    }

    public static String ColorToHex(Color color) {
        String rgb = Integer.toHexString(color.getRGB());
        return "#" + rgb.substring(2, rgb.length());
    }

    public int getLifelineById(String id) {
        int index = -1;
        for (int i = 0; i < p_lifeline_tags.size(); i++) {
            if (p_lifeline_tags.get(i).id.equals(id)) {
                index = i;
                break;
            }
        }
        return index;
    }

    public int getQueueIndexById(String id) {
        int index = -1;
        for (int i = 0; i < queue_places.size(); i++) {
            if (queue_places.get(i).id.equals(id)) {
                index = i;
                break;
            }
        }
        return index;
    }

    public PapyrusCombinedFragment getAltFragmentById(String id) {
        PapyrusCombinedFragment result = null;
        for (Iterator<PapyrusCombinedFragment> iterator = alt_fragments.iterator(); iterator.hasNext();) {
            PapyrusCombinedFragment fragment = iterator.next();
            if (fragment.id.equals(id)) {
                result = fragment;
                break;
            }
        }
        return result;
    }

    public Attr createAttr(Document doc, String name, String value) {
        Attr attr = doc.createAttribute(name);
        attr.setValue(value);
        return attr;
    }

    public String getId() {
        return "_" + (id++);
    }

    public PapyrusMessageTag checkHaveMessageTo(QueuePlace queue_tag) {
        PapyrusMessageTag result = null;
        for (Iterator<PapyrusMessageTag> iterator = p_message_tags.iterator(); iterator.hasNext();) {
            PapyrusMessageTag next = iterator.next();
            if (next.receiveEvent.equals(queue_tag.id)) {
                result = next;
                break;
            };
        }
        return result;
    }

    public boolean isReplyMessage(PapyrusMessageTag message_tag) {
        return (message_tag.messageSort != null && message_tag.messageSort.equals("reply") == true) ? true : false;
    }

    public PapyrusMessageTag findMessageSendFromQueue(QueuePlace queue_tag) {
        PapyrusMessageTag result = null;
        for (Iterator<PapyrusMessageTag> iterator = p_message_tags.iterator(); iterator.hasNext();) {
            PapyrusMessageTag next = iterator.next();
            if (next.sendEvent.equals(queue_tag.id)) {
                result = next;
            };
        }
        return result;
    }

    public int getQueueSendIndexFromQueueRecv(QueuePlace queue_recv) {
        int result = -1;
        int index = queue_tags.indexOf(queue_recv);
        for (int i = index - 1; i >= 0; i--) {
            if (queue_tags.get(i).lifeline_id.equals(queue_recv.lifeline_id)) {
                result = i;
                break;
            }
        }
        return result;
    }

    public Element getPlaceRefToQueue(Document doc, String queue_id) {
        Element result_place = null;
        NodeList places = doc.getElementsByTagName("place");
        for (int i = 0; i < places.getLength(); i++) {
            if (((Element) places.item(i)).getAttribute("queue-ref").equals(queue_id)) {
                result_place = (Element) places.item(i);
                break;
            }
        }
        return result_place;
    }

    public Element getPlaceElementFromId(Document doc, String id) {
        Element result_place = null;
        NodeList places = doc.getElementsByTagName("place");
        for (int i = 0; i < places.getLength(); i++) {
            if (((Element) places.item(i)).getAttribute("id").equals(id)) {
                result_place = (Element) places.item(i);
                break;
            }
        }
        return result_place;
    }

    public SynchOrdinaryPlace checkHaveSynchOrdinaryPlaceFromQueue(QueuePlace from_queue) {
        SynchOrdinaryPlace result = null;
        for (int i = 0; i < sync_ordinary_places.size(); i++) {
            if (sync_ordinary_places.get(i).from_queue == from_queue) {
                result = sync_ordinary_places.get(i);
                break;
            }
        }
        return result;
    }

    private SynchOrdinaryPlace checkHaveSynchOrdinaryPlaceToMiddleQueue(QueuePlace from_queue) {
        SynchOrdinaryPlace result = null;
        for (int i = 0; i < sync_ordinary_places.size(); i++) {
            if (sync_ordinary_places.get(i).middle_queue.id.equals(from_queue.id)) {
                result = sync_ordinary_places.get(i);
                break;
            }
        }
        return result;
    }

    public QueuePlace getNextQueueOfSameLifeline(QueuePlace queue) {
        QueuePlace result = null;
        PapyrusLifelineTag lifeline = p_lifeline_tags.get(getLifelineById(queue.lifeline_id));
        if (lifeline.queues.contains(queue)
                && (lifeline.queues.indexOf(queue) + 1) <= (lifeline.queues.size() - 1)) {
            result = lifeline.queues.get(lifeline.queues.indexOf(queue) + 1);
        }
        return result;
    }

    public int inAlt(QueuePlace queue_place) {
        for (Iterator<PapyrusCombinedFragment> iterator = alt_fragments.iterator(); iterator.hasNext();) {
            PapyrusCombinedFragment alt = iterator.next();
            if (alt.containQueuePlace(queue_place)) {
                return alt_fragments.indexOf(alt);
            }

        }
        return -1;
    }

    public void writeDomQPME() {
        try {

            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
            int i;
            // root elements
            Document doc = docBuilder.newDocument();
            Element rootElement = doc.createElement("net");
            doc.appendChild(rootElement);
            //Attr attr = doc.createAttribute("xmlns:xsi");
            //attr.setValue("http://www.w3.org/2001/XMLSchema-instance");
            rootElement.setAttributeNode(createAttr(doc, "xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance"));
            rootElement.setAttributeNode(createAttr(doc, "qpme-version", "2.1.0"));

            Element colors = doc.createElement("colors");
            rootElement.appendChild(colors);
            Element color;
            for (i = 0; i < color_tags.size(); i++) {
                color = doc.createElement("color");
                color.setAttributeNode(createAttr(doc, "real-color", color_tags.get(i).real_color));
                color.setAttributeNode(createAttr(doc, "id", color_tags.get(i).id));
                color.setAttributeNode(createAttr(doc, "name", color_tags.get(i).name));
                colors.appendChild(color);
            }

            Element queues = doc.createElement("queues");
            rootElement.appendChild(queues);
            ArrayList<Element> al_queue = new ArrayList<Element>();
            Element queue;
            for (i = 0; i < queue_tags.size(); i++) {
                queue = doc.createElement("queue");
                queue.setAttributeNode(createAttr(doc, "strategy", "FCFS"));
                queue.setAttributeNode(createAttr(doc, "number-of-servers", "1"));
                queue.setAttributeNode(createAttr(doc, "id", queue_tags.get(i).id));
                queue.setAttributeNode(createAttr(doc, "name", queue_tags.get(i).name));
                queues.appendChild(queue);
                al_queue.add(queue);
            }

            Element places = doc.createElement("places");
            rootElement.appendChild(places);
            // add transitions 
            Element root_transitions = doc.createElement("transitions");
            rootElement.appendChild(root_transitions);

            // add connections 
            Element root_connections = doc.createElement("connections");
            rootElement.appendChild(root_connections);

            // tim reply message --> de add ordinary-place sync
//            QueuePlace queue_recv;
//            QueuePlace queue_send;
//            QueuePlace middle_queue;
//            PapyrusMessageTag message_send;
//            for (Iterator<PapyrusMessageTag> iterator = p_message_tags.iterator(); iterator.hasNext();) {
//                PapyrusMessageTag message = iterator.next();
//                if (isReplyMessage(message)) {
//                    int index_queue_recv = getQueueIndexById(message.receiveEvent);
//                    queue_recv = queue_places.get(index_queue_recv); // queue2
//                    int index_queue_send = getQueueSendIndexFromQueueRecv(queue_recv);
//                    if (index_queue_send == -1) {
//                        continue;
//                    }
//                    queue_send = queue_places.get(index_queue_send); // queue1
//                    middle_queue = queue_places.get(getQueueIndexById(message.sendEvent));
//                    SynchOrdinaryPlace synchOrdinaryPlace = new SynchOrdinaryPlace(queue_send, queue_recv, middle_queue);
//
//                    String name_place = "Sync" + p_lifeline_tags.get(getLifelineById(queue_send.lifeline_id)).name + (Integer.parseInt(queue_send.name.replaceAll("[\\D]", ""))) + (Integer.parseInt(queue_recv.name.replaceAll("[\\D]", "")));
//                    message_send = findMessageSendFromQueue(queue_send);
//                    if (message_send != null) {
//                        place = doc.createElement("place");
//                        place.setAttributeNode(createAttr(doc, "id", getId()));
//                        synchOrdinaryPlace.place_id = place.getAttribute("id");
//                        sync_ordinary_places.add(synchOrdinaryPlace);
//                        place.setAttributeNode(createAttr(doc, "departure-discipline", "NORMAL"));
//                        place.setAttributeNode(createAttr(doc, "xsi:type", "ordinary-place"));
//                        place.setAttributeNode(createAttr(doc, "name", name_place));
//                        places.appendChild(place);
//                        // add meta-attributes tag
//                        Element meta_attributes = doc.createElement("meta-attributes");
//                        place.appendChild(meta_attributes);
//                        Element meta_attribute = doc.createElement("meta-attribute");
//                        meta_attribute.setAttributeNode(createAttr(doc, "xsi:type", "location-attribute"));
//                        meta_attribute.setAttributeNode(createAttr(doc, "location-x", "190"));
//                        meta_attribute.setAttributeNode(createAttr(doc, "location-y", "127"));
//                        meta_attributes.appendChild(meta_attribute);
//
//                        meta_attribute = doc.createElement("meta-attribute");
//                        meta_attribute.setAttributeNode(createAttr(doc, "xsi:type", "simqpn-place-configuration"));
//                        meta_attribute.setAttributeNode(createAttr(doc, "id", getId()));
//                        meta_attribute.setAttributeNode(createAttr(doc, "configuration-name", "sync-1")); //???
//                        meta_attribute.setAttributeNode(createAttr(doc, "statsLevel", "1"));
//                        meta_attributes.appendChild(meta_attribute);
//
//                        // add color-refs;
//                        Element color_refs = doc.createElement("color-refs");
//                        place.appendChild(color_refs);
//                        Element color_ref = doc.createElement("color-ref");
//                        color_refs.appendChild(color_ref);
//                        color_ref.setAttributeNode(createAttr(doc, "id", getId()));
//                        color_ref.setAttributeNode(createAttr(doc, "color-id", queue_send.lifeline_id));
//                        color_ref.setAttributeNode(createAttr(doc, "maximum-capacity", "0"));
//                        color_ref.setAttributeNode(createAttr(doc, "xsi:type", "ordinary-color-reference"));
//                        color_ref.setAttributeNode(createAttr(doc, "initial-population", "0"));
//
//                    }
//                };
//            }
//
//            // add Transitions
//            // duyet lifeline --> duyet queue (vi moi queue có 1 transition)
//            Element transitions = doc.createElement("transitions");
//            Element parent_connections = doc.createElement("connections"); // connection noi giua place <-> transition
//            rootElement.appendChild(transitions);
//            rootElement.appendChild(parent_connections);
//
//            for (Iterator<PapyrusLifelineTag> iterator = p_lifeline_tags.iterator(); iterator.hasNext();) {
//                PapyrusLifelineTag lifeline = iterator.next();
//                for (i = 0; i < lifeline.queues.size(); i++) {
//                    QueuePlace from_queue = lifeline.queues.get(i);
//                    QueuePlace to_queue = null;
//                    Element transition = doc.createElement("transition");
//                    Element parent_p2t_connection = doc.createElement("connection");
//                    parent_connections.appendChild(parent_p2t_connection);
//                    transitions.appendChild(transition);
//                    String transition_id = getId();
//                    transition.setAttributeNode(createAttr(doc, "id", transition_id));
//                    parent_p2t_connection.setAttributeNode(createAttr(doc, "target-id", transition_id));
//                    transition.setAttributeNode(createAttr(doc, "priority", "0")); //???
//                    transition.setAttributeNode(createAttr(doc, "xsi:type", "immediate-transition")); //???
//                    transition.setAttributeNode(createAttr(doc, "weight", "1.0")); //???
//                    transition.setAttributeNode(createAttr(doc, "name", "t" + from_queue.name));
//
//                    Element meta_attributes = doc.createElement("meta-attributes");
//                    transition.appendChild(meta_attributes);
//                    Element meta_attribute = doc.createElement("meta-attribute");
//                    meta_attributes.appendChild(meta_attribute);
//                    meta_attribute.setAttributeNode(createAttr(doc, "xsi:type", "location-attribute"));
//                    meta_attribute.setAttributeNode(createAttr(doc, "location-x", "259"));
//                    meta_attribute.setAttributeNode(createAttr(doc, "location-y", "127"));
//
//                    Element modes = doc.createElement("modes");
//                    transition.appendChild(modes);
//                    Element mode = doc.createElement("mode");
//                    modes.appendChild(mode);
//                    String mode_id = getId();
//                    mode.setAttributeNode(createAttr(doc, "id", mode_id));
//                    mode.setAttributeNode(createAttr(doc, "real-color", ColorToHex(randomColor())));
//                    mode.setAttributeNode(createAttr(doc, "firing-weight", "1.0"));
//                    mode.setAttributeNode(createAttr(doc, "name", "t" + from_queue.name));
//
//                    Element connections = doc.createElement("connections");
//                    transition.appendChild(connections);
//
//                    Element place_element_of_from_queue = getPlaceElementFromId(doc, from_queue.id);
//                    parent_p2t_connection.setAttributeNode(createAttr(doc, "source-id", place_element_of_from_queue.getAttribute("id")));
//                    PapyrusMessageTag message_to = checkHaveMessageTo(from_queue);
//                    NodeList color_refs_from = null;
//                    color_refs_from = place_element_of_from_queue.getElementsByTagName("color-ref");
//                    for (int j = 0; j < color_refs_from.getLength(); j++) {
//                        Element color_ref = (Element) color_refs_from.item(j);
//                        System.out.println(color_ref.getAttribute("id"));
//                        Element connection = doc.createElement("connection");
//                        connection.setAttributeNode(createAttr(doc, "id", getId()));
//                        connection.setAttributeNode(createAttr(doc, "count", "1")); // number of token - defautl 1
//                        connection.setAttributeNode(createAttr(doc, "source-id", color_ref.getAttribute("id")));
//                        connection.setAttributeNode(createAttr(doc, "target-id", mode_id));
//                        connections.appendChild(connection);
//
//                        if ((i == 0 && color_ref.getAttribute("color-id").equals(lifeline.id))) {
//                            // them connection tu transition -> place dau tien
//                            Element parent_t2p_connection = doc.createElement("connection");
//                            parent_connections.appendChild(parent_t2p_connection);
//                            parent_t2p_connection.setAttributeNode(createAttr(doc, "source-id", transition_id));
//                            parent_t2p_connection.setAttributeNode(createAttr(doc, "target-id", place_element_of_from_queue.getAttribute("id")));
//
//                            // theo connection tu mode -> place dau tien
//                            connection = doc.createElement("connection");
//                            connection.setAttributeNode(createAttr(doc, "id", getId()));
//                            connection.setAttributeNode(createAttr(doc, "count", "1")); // number of token - defautl 1
//                            connection.setAttributeNode(createAttr(doc, "source-id", mode_id));
//                            connection.setAttributeNode(createAttr(doc, "target-id", color_ref.getAttribute("id")));
//                            connections.appendChild(connection);
//                        }
//                    }
//
//                    // check xem co message nao tu queue nay khong?
//                    PapyrusMessageTag message_from = findMessageSendFromQueue(from_queue);
//                    if (message_from != null) {
//                        // co message xuat phat tu queue nay --> tim queue den!
//                        to_queue = queue_places.get(getQueueIndexById(message_from.receiveEvent));
//                        Element parent_t2p_msg_connection = doc.createElement("connection");
//                        parent_connections.appendChild(parent_t2p_msg_connection);
//                        parent_t2p_msg_connection.setAttributeNode(createAttr(doc, "source-id", transition_id));
//                        Element place_element_of_to_queue = getPlaceElementFromId(doc, to_queue.id);
//                        parent_t2p_msg_connection.setAttributeNode(createAttr(doc, "target-id", place_element_of_to_queue.getAttribute("id")));
//                        NodeList color_refs_to = place_element_of_to_queue.getElementsByTagName("color-ref");
//                        boolean flag_get_only_color_of_message = true;
//                        if (message_from.messageSort != null && message_from.messageSort.equals("reply")) {
//                            flag_get_only_color_of_message = false; // flage check for only B2 ref two 2 color-ref of A3
//                        }
//                        for (int j = 0; j < color_refs_to.getLength(); j++) {
//                            Element color_ref = (Element) color_refs_to.item(j);
//                            Element connection = doc.createElement("connection");
//                            connection.setAttributeNode(createAttr(doc, "id", getId()));
//                            connection.setAttributeNode(createAttr(doc, "count", "1")); // number of token - defautl 1
//                            connection.setAttributeNode(createAttr(doc, "source-id", mode_id));
//                            connection.setAttributeNode(createAttr(doc, "target-id", color_ref.getAttribute("id")));
//                            if (flag_get_only_color_of_message && color_ref.getAttribute("color-id").equals(message_from.id)) { // vi ban dau khoi tao color_id = message_i
//                                connections.appendChild(connection);
//                            } else if (flag_get_only_color_of_message == false) {
//                                connections.appendChild(connection);
//                            }
//                        }
//                    }
//                    Element place_element_of_to_queue = null; // place of sync or normal -> from_queue
//                    // check hava synch ordinary place --> queue (A2 -> syncA23)
//                    SynchOrdinaryPlace synchOrdinaryPlace = checkHaveSynchOrdinaryPlaceFromQueue(from_queue);
//                    if (synchOrdinaryPlace != null) {
//                        place_element_of_to_queue = getPlaceElementFromId(doc, synchOrdinaryPlace.place_id);
//                    } else {
//                        to_queue = getNextQueueOfSameLifeline(from_queue);
//                        if (to_queue != null) {
//                            place_element_of_to_queue = getPlaceElementFromId(doc, to_queue.id);
//                        }
//                    }
//
//                    if (place_element_of_to_queue != null) {
//                        Element parent_t2p_sync_or_normal_connection = doc.createElement("connection");
//                        parent_connections.appendChild(parent_t2p_sync_or_normal_connection);
//                        parent_t2p_sync_or_normal_connection.setAttributeNode(createAttr(doc, "source-id", transition_id));
//                        parent_t2p_sync_or_normal_connection.setAttributeNode(createAttr(doc, "target-id", place_element_of_to_queue.getAttribute("id")));
//
//                        NodeList color_refs = place_element_of_to_queue.getElementsByTagName("color-ref");
//                        NodeList color_refs_of_from_queue = place_element_of_from_queue.getElementsByTagName("color-ref");
//                        
//                        // only create connection mode -> color-ref (of to place) with: have color-id same with color-ref of from place
//                        ArrayList<String> color_refs_ids_of_from_queue = new ArrayList<String>();
//                        if (color_refs_of_from_queue != null) {
//                            for (int j = 0; j < color_refs_of_from_queue.getLength(); j++) {
//                                color_refs_ids_of_from_queue.add(((Element) color_refs_of_from_queue.item(j)).getAttribute("color-id"));
//                            }
//                        }
//                        for (int j = 0; j < color_refs.getLength(); j++) {
//                            Element color_ref = (Element) color_refs.item(j);
//                            if (color_refs_ids_of_from_queue.indexOf(color_ref.getAttribute("color-id")) != -1) {
//                                Element connection = doc.createElement("connection");
//                                connection.setAttributeNode(createAttr(doc, "id", getId()));
//                                connection.setAttributeNode(createAttr(doc, "count", "1")); // number of token - defautl 1
//                                connection.setAttributeNode(createAttr(doc, "source-id", mode_id));
//                                connection.setAttributeNode(createAttr(doc, "target-id", color_ref.getAttribute("id")));
//                                connections.appendChild(connection);
//                            }
//                        }
//
//                    }
//
//                    // check hava synch ordinary place --> queue (syncA23->B2)
//                    place_element_of_to_queue = null;
//                    SynchOrdinaryPlace synchOrdinaryPlaceToMiddleQueue = checkHaveSynchOrdinaryPlaceToMiddleQueue(from_queue);
//                    if (synchOrdinaryPlaceToMiddleQueue != null) {
//                        place_element_of_to_queue = getPlaceElementFromId(doc, synchOrdinaryPlaceToMiddleQueue.place_id);
//                    }
//
//                    if (place_element_of_to_queue != null) {
//                        Element parent_t2p_sync_or_normal_connection = doc.createElement("connection");
//                        parent_connections.appendChild(parent_t2p_sync_or_normal_connection);
//                        parent_t2p_sync_or_normal_connection.setAttributeNode(createAttr(doc, "source-id", place_element_of_to_queue.getAttribute("id")));
//                        parent_t2p_sync_or_normal_connection.setAttributeNode(createAttr(doc, "target-id", transition_id));
//
//                        NodeList color_refs = place_element_of_to_queue.getElementsByTagName("color-ref");
//                        for (int j = 0; j < color_refs.getLength(); j++) {
//                            Element color_ref = (Element) color_refs.item(j);
//                            Element connection = doc.createElement("connection");
//                            connection.setAttributeNode(createAttr(doc, "id", getId()));
//                            connection.setAttributeNode(createAttr(doc, "count", "1")); // number of token - defautl 1
//                            connection.setAttributeNode(createAttr(doc, "source-id", color_ref.getAttribute("id")));
//                            connection.setAttributeNode(createAttr(doc, "target-id", mode_id));
//                            connections.appendChild(connection);
//                        }
//                    }
//
//                    if (i == (lifeline.queues.size() - 1)) {
//                        Element parent_t2p_connection = doc.createElement("connection");
//                        parent_connections.appendChild(parent_t2p_connection);
//                        parent_t2p_connection.setAttributeNode(createAttr(doc, "source-id", transition_id));
//                        parent_t2p_connection.setAttributeNode(createAttr(doc, "target-id", place_element_of_from_queue.getAttribute("id")));
//                    }
//                }
//
//            }
            // START WRITE XML OF ALT
            for (Iterator<PapyrusCombinedFragment> iterator = alt_fragments.iterator(); iterator.hasNext();) {

                PapyrusCombinedFragment alt = iterator.next();
                // CREATE PLACE SUBNET
                alt.subnet_place_id = getId();
                Element alt_place = MyNodeLibrary.createElement(doc, "place", 4,
                        "id", alt.subnet_place_id,
                        "departure-discipline", "NORMAL",
                        "xsi:type", "subnet-place",
                        "name", "ALT_" + alt.getAltName());
                places.appendChild(alt_place);
                // add meta-attributes tag
                Element meta_attributes = doc.createElement("meta-attributes");
                alt_place.appendChild(meta_attributes);
                Element meta_attribute = MyNodeLibrary.createElement(doc, "meta-attribute", 3,
                        "xsi:type", "location-attribute",
                        "location-x", "190",
                        "location-y", "127");
                meta_attributes.appendChild(meta_attribute);

                meta_attribute = MyNodeLibrary.createElement(doc, "meta-attribute", 4,
                        "xsi:type", "simqpn-place-configuration",
                        "id", getId(),
                        "configuration-name", "alt-1",
                        "statsLevel", "1");
                meta_attributes.appendChild(meta_attribute);

                // add color-refs;
                Element color_refs = doc.createElement("color-refs");
                alt_place.appendChild(color_refs);
                for (Iterator<PapyrusLifelineTag> iterator1 = alt.covered_lifelines.iterator(); iterator1.hasNext();) {
                    PapyrusLifelineTag lifeline = iterator1.next();
                    String color_ref_id = getId();
                    alt.color_ref_ids.add(color_ref_id);
                    Element color_ref = MyNodeLibrary.createElement(doc, "color-ref", 6,
                            "id", color_ref_id,
                            "color-id", lifeline.id,
                            "initial-population", "0",
                            "maximum-capacity", "0",
                            "xsi:type", "subnet-color-reference",
                            "direction", "both");
                    color_refs.appendChild(color_ref);
                }

                Element subnet = doc.createElement("subnet");
                alt_place.appendChild(subnet);

                Element places_of_subnet = doc.createElement("places");
                Element colors_of_subnet = doc.createElement("colors");
                //places_of_subnet.appendChild(colors_of_subnet);
                subnet.appendChild(places_of_subnet);
                subnet.appendChild(colors_of_subnet);
                // add input-place of  subnet, output place
                alt.input_place.id = getId();
                Element input_place_of_places_subnet = MyNodeLibrary.createElement(doc, "place", 5,
                        "id", alt.input_place.id,
                        "departure-discipline", "NORMAL",
                        "xsi:type", "ordinary-place",
                        "name", "input-place",
                        "locked", "true");
                alt.output_place.id = getId();
                Element out_place_of_places_subnet = MyNodeLibrary.createElement(doc, "place", 5,
                        "id", alt.output_place.id,
                        "departure-discipline", "NORMAL",
                        "xsi:type", "ordinary-place",
                        "name", "output-place",
                        "locked", "true");

                places_of_subnet.appendChild(input_place_of_places_subnet);
                places_of_subnet.appendChild(out_place_of_places_subnet);

                out_place_of_places_subnet.appendChild(createMetaAttributes(doc, 190, 127, "place"));
                input_place_of_places_subnet.appendChild(createMetaAttributes(doc, 190, 12, "place"));

                // add color ref for input-place, output-place
                Element color_refs_input = doc.createElement("color-refs");
                Element color_refs_ouput = doc.createElement("color-refs");

                for (Iterator<PapyrusLifelineTag> iterator1 = alt.covered_lifelines.iterator(); iterator1.hasNext();) {
                    PapyrusLifelineTag lifeline = iterator1.next();
                    String subnet_color_ref_input_place = getId();
                    String subnet_color_ref_output_place = getId();
                    alt.input_place.color_ref_ids.add(subnet_color_ref_input_place);
                    alt.output_place.color_ref_ids.add(subnet_color_ref_output_place);
                    color_refs_ouput.appendChild(createColorRefElement(doc, subnet_color_ref_output_place, lifeline.id, "subnet-color-reference"));
                    color_refs_input.appendChild(createColorRefElement(doc, subnet_color_ref_input_place, lifeline.id, "subnet-color-reference"));
                }

                input_place_of_places_subnet.appendChild(color_refs_input);
                out_place_of_places_subnet.appendChild(color_refs_ouput);
                // add transitions of subnet
                Element subnet_transtions = doc.createElement("transitions");
                subnet.appendChild(subnet_transtions);
                String subnet_transition_do_id = getId();
                Element subnet_transition_do = MyNodeLibrary.createElement(doc, "transition", 5,
                        "id", subnet_transition_do_id,
                        "priority", "0",
                        "xsi:type", "immediate-transition",
                        "weight", "1.0",
                        "name", "tALT_do");
                subnet_transition_do.appendChild(createMetaAttributes(doc, 170, 87, "transition"));
                subnet_transtions.appendChild(subnet_transition_do);
                Element subnet_transition_do_modes = doc.createElement("modes");
                subnet_transition_do.appendChild(subnet_transition_do_modes);
                Element subnet_transition_do_connections = doc.createElement("connections");
                subnet_transition_do.appendChild(subnet_transition_do_connections);

                Element subnet_connections = doc.createElement("connections");
                subnet.appendChild(subnet_connections);
                subnet_connections.appendChild(createConnection(doc, alt.input_place.id, subnet_transition_do_id));
                // create mode of subnet_transition_do for operand;

                // add place ALT_OPERAND
                for (Iterator<PapyrusOperandTag> iterator1 = alt.operands.iterator(); iterator1.hasNext();) {
                    PapyrusOperandTag operand = iterator1.next();
                    // create mode of transition do for operand
                    String mode_operand_id = getId();
                    Element mode_operand = MyNodeLibrary.createElement(doc, "mode", 4,
                            "id", mode_operand_id,
                            "real-color", ColorToHex(randomColor()),
                            "firing-weight", "1.0",
                            "name", "doALT" + (alt.operands.indexOf(operand) + 1));
                    subnet_transition_do_modes.appendChild(mode_operand);
                    for (int index_color_ref_subnet_input_place = 0; index_color_ref_subnet_input_place < alt.input_place.color_ref_ids.size(); index_color_ref_subnet_input_place++) {
                        subnet_transition_do_connections.appendChild(createConnection(doc, alt.input_place.color_ref_ids.get(index_color_ref_subnet_input_place), mode_operand_id));
                    }

                    operand.subnet_place_id = getId();
                    Element subnet_place_op = MyNodeLibrary.createElement(doc, "place", 4,
                            "id", operand.subnet_place_id,
                            "departure-discipline", "NORMAL",
                            "xsi:type", "subnet-place",
                            "name", "ALT_" + alt.getAltName() + "_O" + (alt.operands.indexOf(operand) + 1));
                    places_of_subnet.appendChild(subnet_place_op);
                    subnet_connections.appendChild(createConnection(doc, subnet_transition_do_id, operand.subnet_place_id));

                    // create transition exit for operand;
                    String op_transition_exit_id = getId();
                    Element op_transiton_exit = MyNodeLibrary.createElement(doc, "transition", 5,
                            "id", op_transition_exit_id,
                            "priority", "0",
                            "xsi:type", "immediate-transition",
                            "weight", "1.0",
                            "name", "tALT_exit_O" + (alt.operands.indexOf(operand) + 1));
                    op_transiton_exit.appendChild(createMetaAttributes(doc, 190, 127, "transition"));
                    subnet_transtions.appendChild(op_transiton_exit);
                    subnet_connections.appendChild(createConnection(doc, operand.subnet_place_id, op_transition_exit_id));
                    subnet_connections.appendChild(createConnection(doc, op_transition_exit_id, alt.output_place.id));
                    Element op_transiton_exit_modes = doc.createElement("modes");
                    op_transiton_exit.appendChild(op_transiton_exit_modes);
                    String op_transition_exit_mode_id = getId();
                    op_transiton_exit_modes.appendChild(MyNodeLibrary.createElement(doc, "mode", 4,
                            "id", op_transition_exit_mode_id,
                            "real-color", ColorToHex(randomColor()),
                            "firing-weight", "1.0",
                            "name", "exitO" + (alt.operands.indexOf(operand) + 1)));
                    Element op_transition_exit_connections = doc.createElement("connections");
                    op_transiton_exit.appendChild(op_transition_exit_connections);
                    for (int index_color_ref_subnet_output_place = 0; index_color_ref_subnet_output_place < alt.output_place.color_ref_ids.size(); index_color_ref_subnet_output_place++) {
                        op_transition_exit_connections.appendChild(createConnection(doc, op_transition_exit_mode_id, alt.output_place.color_ref_ids.get(index_color_ref_subnet_output_place)));
                    }

                    // add meta-attributes tag
                    meta_attributes = doc.createElement("meta-attributes");
                    subnet_place_op.appendChild(createMetaAttributes(doc, 190, 127, "place"));

                    // add color-refs;
                    color_refs = doc.createElement("color-refs");
                    subnet_place_op.appendChild(color_refs);
                    for (Iterator<PapyrusLifelineTag> iterator_op = operand.covered_lifelines.iterator(); iterator_op.hasNext();) {
                        PapyrusLifelineTag lifeline = iterator_op.next();
                        String color_ref_id = getId();
                        Element color_ref = MyNodeLibrary.createElement(doc, "color-ref", 6,
                                "id", color_ref_id,
                                "color-id", lifeline.id,
                                "initial-population", "0",
                                "maximum-capacity", "0",
                                "xsi:type", "subnet-color-reference",
                                "direction", "both");
                        color_refs.appendChild(color_ref);

                        //add connection from mode transition do -> color ref
                        subnet_transition_do_connections.appendChild(createConnection(doc, mode_operand_id, color_ref_id));
                        op_transition_exit_connections.appendChild(createConnection(doc, color_ref_id, op_transition_exit_mode_id));
                    }

                    Element subnet_op = doc.createElement("subnet");
                    subnet_place_op.appendChild(subnet_op);

                    Element places_of_subnet_op = doc.createElement("places");
                    Element colors_of_subnet_op = doc.createElement("colors");
                    subnet_op.appendChild(colors_of_subnet_op);
                    subnet_op.appendChild(places_of_subnet_op);
                    // add input-place of  subnet, output place  for alt op
                    operand.input_place.id = getId();
                    Element input_place_of_places_subnet_op = MyNodeLibrary.createElement(doc, "place", 5,
                            "id", operand.input_place.id,
                            "departure-discipline", "NORMAL",
                            "xsi:type", "ordinary-place",
                            "name", "input-place",
                            "locked", "true");
                    operand.output_place.id = getId();
                    Element output_place_of_places_subnet_op = MyNodeLibrary.createElement(doc, "place", 5,
                            "id", operand.output_place.id,
                            "departure-discipline", "NORMAL",
                            "xsi:type", "ordinary-place",
                            "name", "output-place",
                            "locked", "true");
                    places_of_subnet_op.appendChild(input_place_of_places_subnet_op);
                    places_of_subnet_op.appendChild(output_place_of_places_subnet_op);

                    input_place_of_places_subnet_op.appendChild(createMetaAttributes(doc, 190, 127, "place"));
                    output_place_of_places_subnet_op.appendChild(createMetaAttributes(doc, 190, 127, "place"));

                    Element op_color_refs_input = doc.createElement("color-refs");
                    Element op_color_refs_output = doc.createElement("color-refs");
                    input_place_of_places_subnet_op.appendChild(op_color_refs_input);
                    output_place_of_places_subnet_op.appendChild(op_color_refs_output);

                    for (Iterator<PapyrusLifelineTag> iterator2 = operand.covered_lifelines.iterator(); iterator2.hasNext();) {
                        PapyrusLifelineTag lifeline = iterator2.next();
                        String op_color_ref_input_place_id = getId();
                        String op_color_ref_output_place_id = getId();
                        operand.input_place.color_ref_ids.add(op_color_ref_input_place_id);
                        operand.output_place.color_ref_ids.add(op_color_ref_output_place_id);
                        op_color_refs_input.appendChild(createColorRefElement(doc, op_color_ref_input_place_id, lifeline.id, "subnet-color-reference"));
                        op_color_refs_output.appendChild(createColorRefElement(doc, op_color_ref_output_place_id, lifeline.id, "subnet-color-reference"));
                    }
                    // add normal queue-place

                    for (int op_qp_index = 0; op_qp_index < operand.queues.size(); op_qp_index++) {
                        QueuePlace op_queue_place = operand.queues.get(op_qp_index);
                        Element op_place_normal = MyNodeLibrary.createElement(doc, "place", 5,
                                "id", op_queue_place.id,
                                "departure-discipline", "NORMAL",
                                "xsi:type", "queueing-place",
                                "name", op_queue_place.name,
                                "queue-ref", op_queue_place.queue_ref);
                        places_of_subnet_op.appendChild(op_place_normal);
                        // add meta-attributes tag

                        meta_attributes = doc.createElement("meta-attributes");
                        op_place_normal.appendChild(meta_attributes);
                        meta_attribute = MyNodeLibrary.createElement(doc, "meta-attribute", 3,
                                "xsi:type", "location-attribute",
                                "location-x", "190",
                                "location-y", "127");
                        meta_attributes.appendChild(meta_attribute);

                        meta_attribute = MyNodeLibrary.createElement(doc, "meta-attribute", 3,
                                "xsi:type", "simqpn-place-configuration",
                                "id", getId(),
                                "configuration-name", "alt-1",
                                "statsLevel", "3");
                        meta_attributes.appendChild(meta_attribute);

                        // add color-refs;
                        color_refs = doc.createElement("color-refs");
                        op_place_normal.appendChild(color_refs);
                        String color_ref_id = getId();
                        op_queue_place.color_ref_ids.add(color_ref_id);
                        color_refs.appendChild(createColorRefElement(doc, color_ref_id, op_queue_place.lifeline_id, "queueing-color-reference"));

                        // check have message to --> co nhieu color-ref
                        PapyrusMessageTag message_to;
                        if ((message_to = checkHaveMessageTo(op_queue_place)) != null) {
                            color_ref_id = getId();
                            op_queue_place.color_ref_ids.add(color_ref_id);
                            color_refs.appendChild(createColorRefElement(doc, color_ref_id, message_to.id, "queueing-color-reference"));
                        }
                    }

                    // add transitions
                    Element op_transitions = doc.createElement("transitions");
                    Element op_parent_connections = doc.createElement("connections"); // connection noi giua place <-> transition
                    subnet_op.appendChild(op_transitions);
                    subnet_op.appendChild(op_parent_connections);
                    //tALT_do
                    String op_transition_do_id = getId();
                    Element op_transition_do = MyNodeLibrary.createElement(doc, "transition", 5,
                            "id", op_transition_do_id,
                            "priority", "0",
                            "xsi:type", "immediate-transition",
                            "weight", "1.0",
                            "name", "tALT_do");
                    op_transitions.appendChild(op_transition_do);

                    Element meta_attribtues_ = doc.createElement("meta-attributes");
                    op_transition_do.appendChild(createMetaAttributes(doc, 190, 127, "transition"));
                    String op_transition_do_mode_id = getId();
                    op_transition_do.appendChild(createModes(doc, 1, op_transition_do_mode_id, "do"));
                    Element op_transition_do_connections = doc.createElement("connections");
                    op_transition_do.appendChild(op_transition_do_connections);
                    // add connection from input-place to transition_do
                    op_parent_connections.appendChild(createConnection(doc, operand.input_place.id, op_transition_do_id));
                    for (int index_op_color_ref_input_place = 0; index_op_color_ref_input_place < operand.input_place.color_ref_ids.size(); index_op_color_ref_input_place++) {
                        // add connection from input-place to mode_id
                        op_transition_do_connections.appendChild(createConnection(doc, operand.input_place.color_ref_ids.get(index_op_color_ref_input_place), op_transition_do_mode_id));
                    }

                    // transition normal
                    for (int op_lifelie_index = 0; op_lifelie_index < operand.covered_lifelines.size(); op_lifelie_index++) {
                        PapyrusLifelineTag lifeline = operand.covered_lifelines.get(op_lifelie_index);
                        ArrayList<QueuePlace> queue_places_of_lifeline = operand.getQueueOfLifeLineInOperand(lifeline);
                        for (int qp_lifeline_index = 0; qp_lifeline_index < queue_places_of_lifeline.size(); qp_lifeline_index++) {
                            QueuePlace from_queue = queue_places_of_lifeline.get(qp_lifeline_index);
                            if (qp_lifeline_index == 0) {
                                // queue dau tien => co connection tu tALT_do -> queue
                                op_parent_connections.appendChild(createConnection(doc, op_transition_do_id, from_queue.id));
                                for (int index_color_ref_of_from_queue = 0; index_color_ref_of_from_queue < from_queue.color_ref_ids.size(); index_color_ref_of_from_queue++) {
                                    Element color_ref_of_from_queue = findColorRefTagById(doc, from_queue.color_ref_ids.get(index_color_ref_of_from_queue));
                                    if (color_ref_of_from_queue.getAttribute("color-id").equals(from_queue.lifeline_id)) {
                                        op_transition_do_connections.appendChild(createConnection(doc, op_transition_do_mode_id, from_queue.color_ref_ids.get(index_color_ref_of_from_queue)));
                                        break;
                                    }
                                }
                            }

                            QueuePlace to_queue = null;
                            String transition_id = getId();
                            Element op_transition = MyNodeLibrary.createElement(doc, "transition", 5,
                                    "id", transition_id,
                                    "priority", "0",
                                    "xsi:type", "immediate-transition",
                                    "weight", "1.0",
                                    "name", "t" + from_queue.name);
                            op_transitions.appendChild(op_transition);

                            Element modes = doc.createElement("modes");
                            op_transition.appendChild(modes);
                            Element mode = doc.createElement("mode");
                            modes.appendChild(mode);
                            String mode_id = getId();
                            mode.setAttributeNode(createAttr(doc, "id", mode_id));
                            mode.setAttributeNode(createAttr(doc, "real-color", ColorToHex(randomColor())));
                            mode.setAttributeNode(createAttr(doc, "firing-weight", "1.0"));
                            mode.setAttributeNode(createAttr(doc, "name", "t" + from_queue.name));

                            Element connections = doc.createElement("connections");
                            op_transition.appendChild(connections);

                            if (qp_lifeline_index == (queue_places_of_lifeline.size() - 1)) {
                                // add connection from queue to ouput-place
                                op_parent_connections.appendChild(createConnection(doc, transition_id, operand.output_place.id));
                                for (Iterator<String> iterator2 = operand.output_place.color_ref_ids.iterator(); iterator2.hasNext();) {
                                    String color_ref_id = iterator2.next();
                                    Element color_ref = findColorRefTagById(doc, color_ref_id);
                                    if (color_ref.getAttribute("color-id").equals(from_queue.lifeline_id)) {
                                        connections.appendChild(createConnection(doc, mode_id, color_ref_id));
                                    }
                                }

                            }
                            op_parent_connections.appendChild(createConnection(doc, from_queue.id, transition_id));
                            op_transition.appendChild(createMetaAttributes(doc, 259, 127, "transition"));

                            PapyrusMessageTag message_to = checkHaveMessageTo(from_queue);
                            for (int j = 0; j < from_queue.color_ref_ids.size(); j++) {
                                String color_ref_id = from_queue.color_ref_ids.get(j);
                                connections.appendChild(createConnection(doc, color_ref_id, mode_id));
                            }

                            // check xem co message nao tu queue nay khong?
                            PapyrusMessageTag message_from = findMessageSendFromQueue(from_queue);
                            if (message_from != null) {
                                // co message xuat phat tu queue nay --> tim queue den!

                                to_queue = queue_places.get(getQueueIndexById(message_from.receiveEvent));
                                if (from_queue.name.equals("A2")) {
                                    System.out.println("A2 to:" + message_from.receiveEvent);
                                    System.out.println("message_id: " + message_from.id);
                                }

                                op_parent_connections.appendChild(createConnection(doc, transition_id, to_queue.id));
                                boolean flag_get_only_color_of_message = true;
                                if (message_from.messageSort != null && message_from.messageSort.equals("reply")) {
                                    flag_get_only_color_of_message = false; // flage check for only B2 ref two 2 color-ref of A3
                                }

                                for (int j = 0; j < to_queue.color_ref_ids.size(); j++) {
                                    String color_ref_id = to_queue.color_ref_ids.get(j);
                                    System.out.println("color-ref-id: " + color_ref_id);
                                    Element connection = createConnection(doc, mode_id, color_ref_id);
                                    if (flag_get_only_color_of_message && findColorRefTagById(doc, color_ref_id).getAttribute("color-id").equals(message_from.id)) { // vi ban dau khoi tao color_id = message_i
                                        connections.appendChild(connection);
                                    } else if (flag_get_only_color_of_message == false) {
                                        connections.appendChild(connection);
                                    }
                                }
                            }

                            // find to_queue to add connections
                            to_queue = getNextQueueOfSameLifeline(from_queue);
                            if (operand.queues.indexOf(to_queue) == -1) {
                                to_queue = null; // chi xet queue trong operand
                            }
                            ArrayList<String> color_ids_of_from_queue = new ArrayList<String>();
                            for (int j = 0; j < from_queue.color_ref_ids.size(); j++) {
                                Element color_ref = findColorRefTagById(doc, from_queue.color_ref_ids.get(j));
                                color_ids_of_from_queue.add(color_ref.getAttribute("color-id"));
                            }
                            if (to_queue != null) {
                                op_parent_connections.appendChild(createConnection(doc, transition_id,to_queue.id));

                                for (int j = 0; j < to_queue.color_ref_ids.size(); j++) {
                                    Element color_ref = findColorRefTagById(doc, to_queue.color_ref_ids.get(j));
                                    if (color_ids_of_from_queue.indexOf(color_ref.getAttribute("color-id")) != -1) {
                                        connections.appendChild(createConnection(doc, mode_id, color_ref.getAttribute("id")));
                                    }
                                }

                            }
                        }
                    }
                }

                // add place ALT_OPERAND
                // PLACE SUBNET
                // CREATE TRANSITION ALT ENTRY
                alt.tALT_entry_id = getId();
                Element tALT_entry = MyNodeLibrary.createElement(doc, "transition", 5,
                        "id", alt.tALT_entry_id,
                        "priority", "0",
                        "xsi:type", "immediate-transition",
                        "weight", "1.0",
                        "name", "tALT" + alt_fragments.indexOf(alt) + "_entry");
                tALT_entry.appendChild(createMetaAttributes(doc, 288, 186, "transition"));
                alt.tALT_entry_mode_id = getId();
                tALT_entry.appendChild(createModes(doc, 1, alt.tALT_entry_mode_id, "entry"));
                alt.tALT_entry_connections = doc.createElement("connections");
                tALT_entry.appendChild(alt.tALT_entry_connections);

                // TRANSITION ALT ENTRY
                // CREATE TRANSITION ALT EXIT
                alt.tALT_exit_id = getId();

                Element tALT_exit = MyNodeLibrary.createElement(doc, "transition", 5,
                        "id", alt.tALT_exit_id,
                        "priority", "0",
                        "xsi:type", "immediate-transition",
                        "weight", "1.0",
                        "name", "tALT" + alt_fragments.indexOf(alt) + "_exit");
                tALT_exit.appendChild(createMetaAttributes(doc, 288, 186, "transition"));

                alt.tALT_exit_mode_id = getId();
                tALT_exit.appendChild(createModes(doc, 1, alt.tALT_exit_mode_id, "tALT" + alt_fragments.indexOf(alt) + "_exit"));
                alt.tALT_exit_connections = doc.createElement("connections");
                tALT_exit.appendChild(alt.tALT_exit_connections);
                // TRANSITION EXIT

                // add connection tALTentry -> ALT subnet 
               root_connections.appendChild(createConnection(doc, alt.tALT_entry_id, alt.subnet_place_id));
                for (Iterator<String> iterator1 = alt.color_ref_ids.iterator(); iterator1.hasNext();) {
                    String color_ref_id = iterator1.next();
                    alt.tALT_entry_connections.appendChild(createConnection(doc, alt.tALT_entry_mode_id, color_ref_id));
                }

                // add connection ALT subnet -> tALT_exit
                root_connections.appendChild(createConnection(doc, alt.subnet_place_id, alt.tALT_exit_id));
                for (Iterator<String> iterator1 = alt.color_ref_ids.iterator(); iterator1.hasNext();) {
                    String color_ref_id = iterator1.next();
                    alt.tALT_exit_connections.appendChild(createConnection(doc, color_ref_id, alt.tALT_exit_mode_id));
                }

                root_transitions.appendChild(tALT_entry);
                root_transitions.appendChild(tALT_exit);

            }
            // END XML OF ALT

            // add other normal place
            Element place;
            for (Iterator<QueuePlace> iterator = queue_places.iterator(); iterator.hasNext();) {
                QueuePlace queue_place = iterator.next();
                if (inAlt(queue_place) != -1) {
                    continue;
                }

                // ADD PLACE
                place = MyNodeLibrary.createElement(doc, "place", 5,
                        "id", queue_place.id,
                        "departure-discipline", "NORMAL",
                        "xsi:type", "queueing-place",
                        "name", queue_place.name,
                        "queue-ref", queue_place.queue_ref);
                places.appendChild(place);
                // add meta-attributes tag
                Element meta_attributes = doc.createElement("meta-attributes");
                place.appendChild(createMetaAttributes(doc, 190, 127, "place"));

                // add color-refs;
                Element color_refs = doc.createElement("color-refs");
                place.appendChild(color_refs);
                //Element color_ref = doc.createElement("color-ref");
                String color_ref_id = getId();
                queue_place.color_ref_ids.add(color_ref_id);
                color_refs.appendChild(createColorRefElement(doc, color_ref_id, queue_place.lifeline_id, "queueing-color-reference"));
                // check have message to --> co nhieu color-ref
                PapyrusMessageTag message_to;
                if ((message_to = checkHaveMessageTo(queue_place)) != null) {
                    color_ref_id = getId();
                    queue_place.color_ref_ids.add(color_ref_id);
                    color_refs.appendChild(createColorRefElement(doc, color_ref_id, message_to.id, "queueing-color-reference"));
                }

            }

            for (Iterator<PapyrusLifelineTag> iterator = p_lifeline_tags.iterator(); iterator.hasNext();) {
                PapyrusLifelineTag lifeline = iterator.next();
                for (i = 0; i < lifeline.queues.size(); i++) {

                    QueuePlace from_queue = lifeline.queues.get(i);
                    QueuePlace to_queue = null;
                    int alt_index;
                    if ((alt_index = inAlt(from_queue)) != -1) {
                        PapyrusCombinedFragment alt = alt_fragments.get(alt_index);
                        if (alt.getLastQPOfLifeline(from_queue.lifeline_id) == from_queue) {
                            QueuePlace next_queue_place_in_lifeline = null;
                            if (lifeline.queues.indexOf(from_queue) < (lifeline.queues.size() - 1)) {
                                next_queue_place_in_lifeline = lifeline.queues.get(lifeline.queues.indexOf(from_queue) + 1);
                            }
                            if (next_queue_place_in_lifeline != null) {
                                root_connections.appendChild(createConnection(doc, alt.tALT_exit_id, next_queue_place_in_lifeline.id));
                                for (Iterator<String> iterator1 = next_queue_place_in_lifeline.color_ref_ids.iterator(); iterator1.hasNext();) {
                                    String color_ref_id = iterator1.next();
                                    Element color_ref = findColorRefTagById(doc, color_ref_id);
                                    if (color_ref.getAttribute("color-id").equals(next_queue_place_in_lifeline.lifeline_id)) {
                                        alt.tALT_exit_connections.appendChild(createConnection(doc, alt.tALT_exit_mode_id, color_ref_id));
                                    }
                                }
                            }
                        }
                        continue;
                    }

                    // check neu queue tiep theo la queue dau tien trong Alt --> co connection cua queue -> tALT_entry
                    QueuePlace next_queue_place_in_lifeline = null;
                    if (lifeline.queues.indexOf(from_queue) < (lifeline.queues.size() - 1)) {
                        next_queue_place_in_lifeline = lifeline.queues.get(lifeline.queues.indexOf(from_queue) + 1);
                    }
                    if (next_queue_place_in_lifeline != null && (alt_index = inAlt(next_queue_place_in_lifeline)) != -1) {
                        PapyrusCombinedFragment alt = alt_fragments.get(alt_index);
                        if (alt.getFirstQPOfLifeline(from_queue.lifeline_id) == next_queue_place_in_lifeline) {
                            root_connections.appendChild(createConnection(doc, from_queue.id, alt.tALT_entry_id));
                            for (Iterator<String> iterator1 = from_queue.color_ref_ids.iterator(); iterator1.hasNext();) {
                                String color_ref_id = iterator1.next();
                                Element color_ref = findColorRefTagById(doc, color_ref_id);
                                if (color_ref.getAttribute("color-id").equals(from_queue.lifeline_id)) {
                                    alt.tALT_entry_connections.appendChild(createConnection(doc, color_ref_id, alt.tALT_entry_mode_id));
                                    // neu la queue dau tien cua lifeline thi co transition nguoc lai
                                    if (lifeline.queues.indexOf(from_queue) == 0) {
                                        root_connections.appendChild(createConnection(doc, alt.tALT_entry_id, from_queue.id));
                                        alt.tALT_entry_connections.appendChild(createConnection(doc, alt.tALT_entry_mode_id, color_ref_id));
                                    }
                                }
                            }
                            continue;
                        }
                    }

                    Element transition = doc.createElement("transition");
                    root_transitions.appendChild(transition);
                    String transition_id = getId();
                    transition.setAttributeNode(createAttr(doc, "id", transition_id));
                    transition.setAttributeNode(createAttr(doc, "priority", "0")); //???
                    transition.setAttributeNode(createAttr(doc, "xsi:type", "immediate-transition")); //???
                    transition.setAttributeNode(createAttr(doc, "weight", "1.0")); //???
                    transition.setAttributeNode(createAttr(doc, "name", "t" + from_queue.name));

                    transition.appendChild(createMetaAttributes(doc, 259, 127, "transition"));

                    // neu la queue cuoi cung cua lifeline thi co transition nguoc lai
                    if (lifeline.queues.indexOf(from_queue) == (lifeline.queues.size() - 1)) {

                        root_connections.appendChild(createConnection(doc, transition_id, from_queue.id));
                    }

                    String mode_id = getId();
                    transition.appendChild(createModes(doc, 1, mode_id, "t" + from_queue.name));
                    Element connections = doc.createElement("connections");
                    transition.appendChild(connections);

                    root_connections.appendChild(createConnection(doc, from_queue.id, transition_id));
                    PapyrusMessageTag message_to = checkHaveMessageTo(from_queue);
                    NodeList color_refs_from = null;
                    for (int j = 0; j < from_queue.color_ref_ids.size(); j++) {
                        Element color_ref = (Element) findColorRefTagById(doc, from_queue.color_ref_ids.get(j));
                        connections.appendChild(createConnection(doc, color_ref.getAttribute("id"), mode_id));

                        if ((i == 0 && color_ref.getAttribute("color-id").equals(lifeline.id))) {
                            // them connection tu transition -> place dau tien
                            root_connections.appendChild(createConnection(doc, transition_id, from_queue.id));
                            // theo connection tu mode -> place dau tien
                            connections.appendChild(createConnection(doc, mode_id, color_ref.getAttribute("id")));
                        }
                    }

                    // check xem co message nao tu queue nay khong?
                    PapyrusMessageTag message_from = findMessageSendFromQueue(from_queue);
                    if (message_from != null) {
                        // co message xuat phat tu queue nay --> tim queue den!
                        to_queue = queue_places.get(getQueueIndexById(message_from.receiveEvent));
                        root_connections.appendChild(createConnection(doc, transition_id, to_queue.id));
                        boolean flag_get_only_color_of_message = true;
                        if (message_from.messageSort != null && message_from.messageSort.equals("reply")) {
                            flag_get_only_color_of_message = false; // flage check for only B2 ref two 2 color-ref of A3
                        }
                        for (int j = 0; j < to_queue.color_ref_ids.size(); j++) {
                            Element color_ref = (Element) findColorRefTagById(doc, to_queue.color_ref_ids.get(j));
                            Element connection = createConnection(doc, mode_id, color_ref.getAttribute("id"));
                            if (flag_get_only_color_of_message && color_ref.getAttribute("color-id").equals(message_from.id)) { // vi ban dau khoi tao color_id = message_i
                                connections.appendChild(connection);
                            } else if (flag_get_only_color_of_message == false) {
                                connections.appendChild(connection);
                            }
                        }
                    }

                    to_queue = getNextQueueOfSameLifeline(from_queue);
                    ArrayList<String> color_ids_of_from_queue = new ArrayList<String>();
                    for (int j = 0; j < from_queue.color_ref_ids.size(); j++) {
                        Element color_ref = findColorRefTagById(doc, from_queue.color_ref_ids.get(j));
                        color_ids_of_from_queue.add(color_ref.getAttribute("color-id"));
                    }

                    if (to_queue != null) {
                        root_connections.appendChild(createConnection(doc, transition_id, to_queue.id));
                        for (int j = 0; j < to_queue.color_ref_ids.size(); j++) {
                            Element color_ref = (Element) findColorRefTagById(doc, to_queue.color_ref_ids.get(j));
                            if (color_ids_of_from_queue.indexOf(color_ref.getAttribute("color-id")) != -1) {
                                connections.appendChild(createConnection(doc, mode_id, color_ref.getAttribute("id")));
                            }
                        }
                    }
                }
            }

            Element meta_attributes = doc.createElement("meta-attributes");
            rootElement.appendChild(meta_attributes);
            Element meta_attribute = doc.createElement("meta-attribute");
            meta_attributes.appendChild(meta_attribute);
            meta_attribute.setAttributeNode(createAttr(doc, "xsi:type", "simqpn-configuration"));
            meta_attribute.setAttributeNode(createAttr(doc, "id", getId()));
            meta_attribute.setAttributeNode(createAttr(doc, "scenario", "1"));
            meta_attribute.setAttributeNode(createAttr(doc, "stopping-rule", "FIXEDLEN"));
            meta_attribute.setAttributeNode(createAttr(doc, "time-before-initial-heart-beat", "100000"));
            meta_attribute.setAttributeNode(createAttr(doc, "time-between-stop-checks", "100000"));
            meta_attribute.setAttributeNode(createAttr(doc, "seconds-between-stop-checks", "60"));
            meta_attribute.setAttributeNode(createAttr(doc, "seconds-between-heart-beats", "60"));
            meta_attribute.setAttributeNode(createAttr(doc, "verbosity-level", "0"));
            meta_attribute.setAttributeNode(createAttr(doc, "output-directory", "."));
            meta_attribute.setAttributeNode(createAttr(doc, "configuration-name", "sync-1"));
            meta_attribute.setAttributeNode(createAttr(doc, "ramp-up-length", "1200.0"));
            meta_attribute.setAttributeNode(createAttr(doc, "total-run-length", "13200.0"));
            // write the content into xml file
            writeToFile(doc);
        } catch (ParserConfigurationException pce) {
            pce.printStackTrace();
        }
    }

    public void writeToFile(Document doc) {
        try {
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(new File("E:\\file.qpe"));

            // Output to console for testing
            // StreamResult result = new StreamResult(System.out);
            transformer.transform(source, result);

            System.out.println("File saved!");
        } catch (TransformerConfigurationException ex) {
            Logger.getLogger(AltReadPapyrus.class.getName()).log(Level.SEVERE, null, ex);
        } catch (TransformerException ex) {
            Logger.getLogger(AltReadPapyrus.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Element createColorRefElement(Document doc, String id, String color_id, String type) {
        Element color_ref = null;
        if (type.equals("subnet-color-reference")) {
            color_ref = MyNodeLibrary.createElement(doc, "color-ref", 6,
                    "id", id,
                    "color-id", color_id,
                    "initial-population", "0",
                    "maximum-capacity", "0",
                    "xsi:type", "subnet-color-reference",
                    "direction", "both");

            Element meta_attributes = doc.createElement("meta-attributes");
            color_ref.appendChild(meta_attributes);
            Element meta_attribute = MyNodeLibrary.createElement(doc, "meta-attribute", 20,
                    "xsi:type", "simqpn-batch-means-color-configuration",
                    "id", getId(),
                    "configuration-name", "alt-1",
                    "statsLevel", "1",
                    "signLev", "0.05",
                    "reqAbsPrc", "50",
                    "reqRelPrc", "0.05",
                    "batchSize", "200",
                    "minBatches", "60",
                    "numBMeansCorlTested", "50",
                    "bucketSize", "100.0",
                    "maxBuckets", "1000",
                    "queueSignLev", "0.05",
                    "queueReqAbsPrc", "50",
                    "queueReqRelPrc", "0.05",
                    "queueBatchSize", "200",
                    "queueMinBatches", "60",
                    "queueNumBMeansCorlTested", "50",
                    "queueBucketSize", "100.0",
                    "queueMaxBuckets", "1000");
            meta_attributes.appendChild(meta_attribute);
        } else if (type.equals("queueing-color-reference")) {
            color_ref = MyNodeLibrary.createElement(doc, "color-ref", 9,
                    "id", id,
                    "color-id", color_id,
                    "maximum-capacity", "0",
                    "xsi:type", "queueing-color-reference",
                    "ranking", "0",
                    "priority", "0",
                    "distribution-function", "Exponential",
                    "lambda", "0.01",
                    "initial-population", "1");

            Element meta_attributes = doc.createElement("meta-attributes");
            color_ref.appendChild(meta_attributes);
            Element meta_attribute = MyNodeLibrary.createElement(doc, "meta-attribute", 15,
                    "id", getId(),
                    "xsi:type", "simqpn-batch-means-queueing-color-configuration",
                    "configuration-name", "alt-1",
                    "signLev", "0.05",
                    "reqAbsPrc", "50",
                    "reqRelPrc", "0.05",
                    "batchSize", "200",
                    "minBatches", "60",
                    "numBMeansCorlTested", "50",
                    "bucketSize", "100.0",
                    "maxBuckets", "1000",
                    "queueSignLev", "0.05",
                    "queueReqAbsPrc", "50",
                    "queueBucketSize", "100.0",
                    "queueMaxBuckets", "1000");
            meta_attributes.appendChild(meta_attribute);
        }
        return color_ref;
    }

    public Element createMetaAttributes(Document doc, int x, int y, String of) {
        Element meta_attributes = doc.createElement("meta-attributes");
        meta_attributes.appendChild(MyNodeLibrary.createElement(doc, "meta-attribute", 3,
                "xsi:type", "location-attribute",
                "location-x", String.valueOf(x),
                "location-y", String.valueOf(y)));
        if (of.equals("place")) {
            meta_attributes.appendChild(MyNodeLibrary.createElement(doc, "meta-attribute", 4,
                    "xsi:type", "simqpn-place-configuration",
                    "id", getId(),
                    "configuration-name", "alt-1",
                    "statsLevel", "1"));
        }

        return meta_attributes;
    }

    public Element findColorRefTagById(Document doc, String id) {
        NodeList color_ref_list = doc.getElementsByTagName("color-ref");
        for (int i = 0; i < color_ref_list.getLength(); i++) {
            Element color_ref = (Element) color_ref_list.item(i);
            if (color_ref.getAttribute("id").equals(id)) {
                return color_ref;
            }
        }
        return null;
    }

    public Element createConnection(Document doc, String source_id, String target_id) {
        Element connection = doc.createElement("connection");
        connection.setAttributeNode(createAttr(doc, "id", getId()));
        connection.setAttributeNode(createAttr(doc, "count", "1"));
        connection.setAttributeNode(createAttr(doc, "source-id", source_id));
        connection.setAttributeNode(createAttr(doc, "target-id", target_id));
        return connection;
    }

    public Element createModes(Document doc, int number_mode, String... args) {
        Element modes = doc.createElement("modes");
        for (int i = 0; i < number_mode; i++) {
            modes.appendChild(MyNodeLibrary.createElement(doc, "mode", 4,
                    "id", args[i * 2],
                    "real-color", ColorToHex(randomColor()),
                    "firing-weight", "1.0",
                    "name", args[i * 2 + 1]));
        }
        return modes;
    }
}
