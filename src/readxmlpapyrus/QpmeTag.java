/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package readxmlpapyrus;

import java.util.ArrayList;

/**
 *
 * @author Nguyen Dinh Manh
 */
class QpmeTag {
    public String id;
    public String name;
    public String type;
}

class QpmeCologTag extends QpmeTag{
    public String real_color;
}

class QpmeQueueTag extends QpmeTag{
    public String strategy;
    public String number_of_servers;
    public String lifeline_id; // index in lifeline_tags
}

class QueuePlace extends QpmeTag{
    public String queue_ref;
    public String lifeline_id; // index in lifeline_tags
    public ArrayList<String> color_ref_ids = new ArrayList<String>();
}

class SynchOrdinaryPlace extends QpmeTag {
    // dai dien cho synch ordinal place dong bo giua queue1 & queue2 thong  qua queue
    QueuePlace from_queue;
    QueuePlace to_queue;
    QueuePlace middle_queue;
    public String place_id; // id of place in qpme xml file
    public SynchOrdinaryPlace(QueuePlace queue_send, QueuePlace queue_recv, QueuePlace midde_queue) {
        this.from_queue = queue_send;
        this.to_queue = queue_recv;
        this.middle_queue = midde_queue;
    }
}
