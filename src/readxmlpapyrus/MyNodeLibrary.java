/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package readxmlpapyrus;

import java.util.HashMap;
import java.util.Map;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 *
 * @author Nguyen Dinh Manh
 */
public class MyNodeLibrary {
    public static enum Tag_Code {
        NONE,
        UML_MODEL,
        PACKAGE_ELEMENT,
        LIFELINE,
        FRAGMENT,
        MESSAGE
    };
    
    public static enum Fragement_Type_Code {
        NONE,
        ExecutionOccurrenceSpecification,
        ActionExecutionSpecification,
        MessageOccurrenceSpecification,
        CombinedFragment,
    }
    
    public static enum  Interaction_Operator_Code {
        NONE,
        ALT,
    }
    
    public static enum Attribute_Code {
        NONE,
        ID, 
        TYPE,
        NAME,
        COVEREDBY,
        RECEICVE_EVENT,
        SEND_EVENT,
        MESSAGE_SORT,
        COVERED,
    }
    
    public static Map<String,Tag_Code> Tag_Name_Map = new HashMap<String, Tag_Code>();
    public static Map<String, Fragement_Type_Code> Frament_Type_Map = new HashMap<String, Fragement_Type_Code>();
    public static Map<String, Attribute_Code> Attribute_Map = new HashMap<String, Attribute_Code>();
    public static Map<String, Interaction_Operator_Code> Interaction_Operator_Map = new HashMap<String, Interaction_Operator_Code>();
    public static void init() {
        Tag_Name_Map.put("uml:Model", Tag_Code.UML_MODEL);
        Tag_Name_Map.put("packagedElement", Tag_Code.PACKAGE_ELEMENT);
        Tag_Name_Map.put("lifeline", Tag_Code.LIFELINE);
        Tag_Name_Map.put("fragment", Tag_Code.FRAGMENT);
        Tag_Name_Map.put("message", Tag_Code.MESSAGE);
        
        Frament_Type_Map.put("uml:ExecutionOccurrenceSpecification", Fragement_Type_Code.ExecutionOccurrenceSpecification);
        Frament_Type_Map.put("uml:ActionExecutionSpecification", Fragement_Type_Code.ActionExecutionSpecification);
        Frament_Type_Map.put("uml:MessageOccurrenceSpecification", Fragement_Type_Code.MessageOccurrenceSpecification);
        Frament_Type_Map.put("uml:CombinedFragment", Fragement_Type_Code.CombinedFragment);
    
        Attribute_Map.put("xmi:id", Attribute_Code.ID);
        Attribute_Map.put("xmi:type", Attribute_Code.TYPE);
        Attribute_Map.put("name", Attribute_Code.NAME);
        Attribute_Map.put("coveredBy", Attribute_Code.COVEREDBY);
        Attribute_Map.put("sendEvent", Attribute_Code.SEND_EVENT);
        Attribute_Map.put("receiveEvent", Attribute_Code.RECEICVE_EVENT);
        Attribute_Map.put("messageSort", Attribute_Code.MESSAGE_SORT);
        Attribute_Map.put("covered", Attribute_Code.COVERED);
    
        Interaction_Operator_Map.put("alt", Interaction_Operator_Code.ALT);
    }
    
    public static Tag_Code getTagCode (String tag_name) {
        return Tag_Name_Map.containsKey(tag_name)? Tag_Name_Map.get(tag_name): Tag_Code.NONE;
    }
    public static Fragement_Type_Code getFragmentTypeCode (String fragment_type) {
        return Frament_Type_Map.containsKey(fragment_type)? Frament_Type_Map.get(fragment_type): Fragement_Type_Code.NONE;
    }
    
    public static Attribute_Code getAttributeCode (String attr_name){
        return Attribute_Map.containsKey(attr_name)? Attribute_Map.get(attr_name): Attribute_Code.NONE;
    }
    
    public static Interaction_Operator_Code getInteractionOperatorCode(String operator_name){
        return Interaction_Operator_Map.containsKey(operator_name)? Interaction_Operator_Map.get(operator_name): Interaction_Operator_Code.NONE;
    }
    
    public static Element createElement(Document doc, String name, int number_properties, String... args){
        Element result = doc.createElement(name);
        for(int i = 0; i < number_properties; i++) {
            result.setAttributeNode(createAttr(doc, args[i*2], args[i*2+1]));
        }
        return result;
    }
    
    public static Attr createAttr(Document doc, String name, String value) {
        Attr attr = doc.createAttribute(name);
        attr.setValue(value);
        return attr;
    }
}
