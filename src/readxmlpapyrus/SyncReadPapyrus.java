/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package readxmlpapyrus;

import java.awt.Color;
import java.io.File;
import java.io.IOException;
import java.security.KeyStore;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author manhnd
 */
public class SyncReadPapyrus {

    /**
     * @param args the command line arguments
     */
    public ArrayList<Color> colors = new ArrayList<Color>();
    public ArrayList<PapyrusLifelineTag> p_lifeline_tags = new ArrayList<PapyrusLifelineTag>();
    public ArrayList<PapyrusMessageTag> p_message_tags = new ArrayList<PapyrusMessageTag>();
    public ArrayList<QpmeCologTag> color_tags = new ArrayList<QpmeCologTag>();
    public ArrayList<QueuePlace> queue_places = new ArrayList<QueuePlace>(); // tuong ung places trong qpme
    public ArrayList<QpmeQueueTag> queue_tags = new ArrayList<QpmeQueueTag>(); // tuong ung queues trong qpme

    public ArrayList<SynchOrdinaryPlace> sync_ordinary_places = new ArrayList<SynchOrdinaryPlace>();
    public int id = 0;

    public static void main(String[] args) {
        // TODO code application logic here
        SyncReadPapyrus f = new SyncReadPapyrus();
        try {
            f.parseNow();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void parseNow() throws ParserConfigurationException, SAXException,
            IOException {
        // Tạo một đối tượng DocumentBuilderFactory từ method tĩnh của nó
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

        // Sét đặt việc kiểm tra tính hợp lệ của tài liệu sẽ phân tích sau này.
        factory.setValidating(true);

        // Tạo đối tượng DocumentBuilder
        DocumentBuilder builder = factory.newDocumentBuilder();

        // Lay ra nut Document (mo ta toan bo tai lieu xml cua file xmlFile.xml
        Document xmlDoc = builder.parse("E:\\GR\\UML\\ReadXmlPapyrus\\src\\readxmlpapyrus\\model.uml");
        // Tu nut Document xmlDoc co the lay ra phan tu goc cua tai lieu XML
        Element root = (Element) xmlDoc.getDocumentElement();
        // In ra man hinh ten cua phan tu goc cua tai lieu XML xmlFile.xml la gi
        //System.out.println("Root name: " + root.getNodeName());
        MyNodeLibrary.init();
        processNode(root);
//        System.out.println(colors.size());
//        for (Iterator<Color> iterator = colors.iterator(); iterator.hasNext();) {
//            Color next = iterator.next();
//            System.out.println(ColorToHex(next));
//        }
//        System.out.println("lifeline size: " + p_lifeline_tags.size());
//        for (Iterator<PapyrusLifelineTag> iterator = p_lifeline_tags.iterator(); iterator.hasNext();) {
//            PapyrusLifelineTag next = iterator.next();
//            System.out.println(next.name);
//        }
        for (Iterator<QueuePlace> iterator = queue_places.iterator(); iterator.hasNext();) {
            QueuePlace next = iterator.next();
            System.out.println(next.name);
        }
        
        writeDomQPME();
    }

    private void processNode(Element element) {
        /**
         * In ra ten cua nut bang myNode.getNodeName() .Voi element ta co the
         * thay boi myElement.getTagName() cung co duoc ket qua tuong tu .Trong
         * tinh huong muon dieu khien viec in ra cac tiep dau ngu (prefix) nen
         * su dung cac method cung cap boi interface Element.
         */
        //print("<" + element.getNodeName());
        // Lay ra danh sach cac thuoc tinh cua Element element
        
        QueuePlace new_QueuePlace;
        QpmeQueueTag new_Queue;
        switch (MyNodeLibrary.getTagCode(element.getNodeName())) {
            case LIFELINE:
                PapyrusLifelineTag new_lifeline = new PapyrusLifelineTag();
                NamedNodeMap nodeMap = element.getAttributes();
                for (int i = 0; i < nodeMap.getLength(); i++) {
                    // Lay ra nut thu i trong tap hop
                    Attr at = (Attr) nodeMap.item(i);
                    //print(" " + at.getNodeName() + "=\"" + at.getNodeValue() + "\"");
                    switch (MyNodeLibrary.getAttributeCode(at.getNodeName())) {
                        case ID:
                            new_lifeline.id = at.getNodeValue();
                            break;
                        case NAME:
                            new_lifeline.name = at.getNodeValue();
                            break;
                        case TYPE:
                            new_lifeline.type = at.getNodeValue();
                            break;
                        case COVEREDBY:
                            new_lifeline.coveredBy = at.getNodeValue();
                            break;
                    }
                }
                new_lifeline.color = addColor();
                p_lifeline_tags.add(new_lifeline);

                // add color tag
                QpmeCologTag new_color = new QpmeCologTag();
                new_color.real_color = new_lifeline.color;
                new_color.id = new_lifeline.id;
                new_color.name = new_lifeline.name.toLowerCase();
                color_tags.add(new_color);

                // add new "queue"
                new_Queue = new QpmeQueueTag();
                new_Queue.id = getId();
                new_Queue.name = new_lifeline.name;
                queue_tags.add(new_Queue);

                new_lifeline.queue_id = new_Queue.id;

                // add new "queue place" 
                new_QueuePlace = new QueuePlace();
                new_QueuePlace.id = getId();
                new_QueuePlace.name = new_lifeline.name;
                new_QueuePlace.lifeline_id = new_lifeline.id;
                new_QueuePlace.queue_ref = new_Queue.id;
                new_lifeline.queues.add(new_QueuePlace);
                queue_places.add(new_QueuePlace);

                break;
            case MESSAGE:
                PapyrusMessageTag new_MessageTag = new PapyrusMessageTag();
                nodeMap = element.getAttributes();
                for (int i = 0; i < nodeMap.getLength(); i++) {
                    // Lay ra nut thu i trong tap hop
                    Attr at = (Attr) nodeMap.item(i);
                    //print(" " + at.getNodeName() + "=\"" + at.getNodeValue() + "\"");
                    switch (MyNodeLibrary.getAttributeCode(at.getNodeName())) {
                        case ID:
                            new_MessageTag.id = at.getNodeValue();
                            break;
                        case NAME:
                            new_MessageTag.name = at.getNodeValue();
                            break;
                        case TYPE:
                            new_MessageTag.type = at.getNodeValue();
                            break;
                        case RECEICVE_EVENT:
                            new_MessageTag.receiveEvent = at.getNodeValue();
                            break;
                        case SEND_EVENT:
                            new_MessageTag.sendEvent = at.getNodeValue();
                            break;
                        case MESSAGE_SORT:
                            new_MessageTag.messageSort = at.getNodeValue();
                            break;
                    }
                }
                new_MessageTag.color = addColor();
                p_message_tags.add(new_MessageTag);

                // add color tag
                new_color = new QpmeCologTag();
                new_color.real_color = new_MessageTag.color;
                new_color.id = new_MessageTag.id;
                new_color.name = new_MessageTag.name.toLowerCase();
                color_tags.add(new_color);


                break;
            case FRAGMENT:
                switch (MyNodeLibrary.getFragmentTypeCode(element.getAttribute("xmi:type"))) {
                    case ExecutionOccurrenceSpecification: // dai dien cho 1 excution occurece ==> tuong ung voi 1 queue
                        new_QueuePlace = new QueuePlace();
                        new_QueuePlace.id = element.getAttribute("xmi:id");
                        String covered = element.getAttribute("covered");
                        int index;
                        if ((index = getLifelineById(covered)) != -1) {
                            // tim thay lifeline covered
                            new_QueuePlace.name = p_lifeline_tags.get(index).name + p_lifeline_tags.get(index).index_ocurrence;
                            new_QueuePlace.lifeline_id = p_lifeline_tags.get(index).id;
                            new_QueuePlace.queue_ref = p_lifeline_tags.get(index).queue_id;
                            p_lifeline_tags.get(index).queues.add(new_QueuePlace);
                            p_lifeline_tags.get(index).index_ocurrence++;
                        }
                        queue_places.add(new_QueuePlace);
                        break;
                    case ActionExecutionSpecification: // dai dien cho 1 excution
                        break;
                    case MessageOccurrenceSpecification: // dai dienj cho 1 message occurrence ==> tuong ung 1 queue
                        new_QueuePlace = new QueuePlace();
                        new_QueuePlace.id = element.getAttribute("xmi:id");
                        covered = element.getAttribute("covered");
                        if ((index = getLifelineById(covered)) != -1) {
                            // tim thay lifeline covered
                            new_QueuePlace.name = p_lifeline_tags.get(index).name + p_lifeline_tags.get(index).index_ocurrence;
                            new_QueuePlace.lifeline_id = p_lifeline_tags.get(index).id;
                            new_QueuePlace.queue_ref = p_lifeline_tags.get(index).queue_id;
                            p_lifeline_tags.get(index).queues.add(new_QueuePlace);
                            p_lifeline_tags.get(index).index_ocurrence++;
                        }
                        queue_places.add(new_QueuePlace);
                        break;
                    default:
                        break;
                }
//                NamedNodeMap nodeMap = element.getAttributes();
//                for (int i = 0; i < nodeMap.getLength(); i++) {
//                    // Lay ra nut thu i trong tap hop
//                    Attr at = (Attr) nodeMap.item(i);
//                    print(" " + at.getNodeName() + "=\"" + at.getNodeValue() + "\"");
//                }
                break;
        }
        // print(">");
        // Lay ra danh sach cac nut con cua nut Element element
        NodeList nodeList = element.getChildNodes();
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node node = nodeList.item(i);
            short type = node.getNodeType();
            switch (type) {
                // Truong hop nut con thu i la mot Element
                case Node.ELEMENT_NODE:
                    processNode((Element) node);// goi method nay cho Element
                    break;
                case Node.COMMENT_NODE:
                    //print("<!-- " + node.getNodeValue() + " -->");
                    break;
                default:
                    /**
                     * Trong cac truong hop nut con la mot kieu nut khac .... De
                     * don gian trong vi du nay khong demo cac truong hop do
                     * .Tuy nhien se co mot vi du demo ve viec su ly toan bo cac
                     * kieu nut trong tai lieu xml .
                     */
                    // print("Other node.....");
                    break;
            }
        }
    }

    /**
     * Su dung trong method processNode(...)
     */
    private void print(String s) {
        System.out.println(s);
    }

    public String addColor() {
        Color new_color = randomColor();
        while (colors.contains(new_color) == true) {
            new_color = randomColor();
        }
        colors.add(new_color);
        return ColorToHex(new_color);
    }

    public Color randomColor() {
        Random random = new Random(); // Probably really put this somewhere where it gets executed only once
        int red = random.nextInt(256);
        int green = random.nextInt(256);
        int blue = random.nextInt(256);
        return new Color(red, green, blue);
    }

    public static String ColorToHex(Color color) {
        String rgb = Integer.toHexString(color.getRGB());
        return "#" + rgb.substring(2, rgb.length());
    }

    public int getLifelineById(String id) {
        int index = -1;
        for (int i = 0; i < p_lifeline_tags.size(); i++) {
            if (p_lifeline_tags.get(i).id.equals(id)) {
                index = i;
                break;
            }
        }
        return index;
    }

    public int getQueueIndexById(String id) {
        int index = -1;
        for (int i = 0; i < queue_places.size(); i++) {
            if (queue_places.get(i).id.equals(id)) {
                index = i;
                break;
            }
        }
        return index;
    }

    public Attr createAttr(Document doc, String name, String value) {
        Attr attr = doc.createAttribute(name);
        attr.setValue(value);
        return attr;
    }

    public String getId() {
        return "_" + (id++);
    }

    public PapyrusMessageTag checkHaveMessageTo(QueuePlace queue_place) {
        PapyrusMessageTag result = null;
        for (Iterator<PapyrusMessageTag> iterator = p_message_tags.iterator(); iterator.hasNext();) {
            PapyrusMessageTag next = iterator.next();
            if (next.receiveEvent.equals(queue_place.id)) {
                result = next;
                break;
            };
        }
        return result;
    }

    public boolean isReplyMessage(PapyrusMessageTag message_tag) {
        return (message_tag.messageSort != null && message_tag.messageSort.equals("reply") == true) ? true : false;
    }

    public PapyrusMessageTag findMessageSendFromQueue(QueuePlace queue_place) {
        PapyrusMessageTag result = null;
        for (Iterator<PapyrusMessageTag> iterator = p_message_tags.iterator(); iterator.hasNext();) {
            PapyrusMessageTag next = iterator.next();
            if (next.sendEvent.equals(queue_place.id)) {
                result = next;
            };
        }
        return result;
    }

    public int getQueueSendIndexFromQueueRecv(QueuePlace queue_recv) {
        int result = -1;
        int index = queue_places.indexOf(queue_recv);
        for (int i = index - 1; i >= 0; i--) {
            if (queue_places.get(i).lifeline_id.equals(queue_recv.lifeline_id)) {
                result = i;
                break;
            }
        }
        return result;
    }

    public Element getPlaceElementFromId(Document doc, String id) {
        Element result_place = null;
        NodeList places = doc.getElementsByTagName("place");
        for (int i = 0; i < places.getLength(); i++) {
            if (((Element) places.item(i)).getAttribute("id").equals(id)) {
                result_place = (Element) places.item(i);
                break;
            }
        }
        return result_place;
    }


    public SynchOrdinaryPlace checkHaveSynchOrdinaryPlaceFromQueue(QueuePlace from_queue) {
        SynchOrdinaryPlace result = null;
        for (int i = 0; i < sync_ordinary_places.size(); i++) {
            if (sync_ordinary_places.get(i).from_queue == from_queue) {
                result = sync_ordinary_places.get(i);
                break;
            }
        }
        return result;
    }

    private SynchOrdinaryPlace checkHaveSynchOrdinaryPlaceToMiddleQueue(QueuePlace from_queue) {
        SynchOrdinaryPlace result = null;
        for (int i = 0; i < sync_ordinary_places.size(); i++) {
            if (sync_ordinary_places.get(i).middle_queue.id.equals(from_queue.id)) {
                System.out.println(sync_ordinary_places.get(i).middle_queue.name);
                System.out.println(from_queue.name);
                result = sync_ordinary_places.get(i);
                break;
            }
        }
        return result;
    }

    public QueuePlace getNextQueueOfSameLifeline(QueuePlace queue) {
        QueuePlace result = null;
        PapyrusLifelineTag lifeline = p_lifeline_tags.get(getLifelineById(queue.lifeline_id));
        if (lifeline.queues.contains(queue)
                && (lifeline.queues.indexOf(queue) + 1) <= (lifeline.queues.size() - 1)) {
            result = lifeline.queues.get(lifeline.queues.indexOf(queue) + 1);
        }
        return result;
    }

    public void writeDomQPME() {
        try {

            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
            int i;
            // root elements
            Document doc = docBuilder.newDocument();
            Element rootElement = doc.createElement("net");
            doc.appendChild(rootElement);
            //Attr attr = doc.createAttribute("xmlns:xsi");
            //attr.setValue("http://www.w3.org/2001/XMLSchema-instance");
            rootElement.setAttributeNode(createAttr(doc, "xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance"));
            rootElement.setAttributeNode(createAttr(doc, "qpme-version", "2.1.0"));

            Element colors = doc.createElement("colors");
            rootElement.appendChild(colors);
            Element color;
            for (i = 0; i < color_tags.size(); i++) {
                color = doc.createElement("color");
                color.setAttributeNode(createAttr(doc, "real-color", color_tags.get(i).real_color));
                color.setAttributeNode(createAttr(doc, "id", color_tags.get(i).id));
                color.setAttributeNode(createAttr(doc, "name", color_tags.get(i).name));
                colors.appendChild(color);
            }

            Element queues = doc.createElement("queues");
            rootElement.appendChild(queues);
            ArrayList<Element> al_queue = new ArrayList<Element>();
            Element queue;
            for (i = 0; i < queue_tags.size(); i++) {
                queue = doc.createElement("queue");
                queue.setAttributeNode(createAttr(doc, "strategy", "FCFS"));
                queue.setAttributeNode(createAttr(doc, "number-of-servers", "1"));
                queue.setAttributeNode(createAttr(doc, "id", queue_tags.get(i).id));
                queue.setAttributeNode(createAttr(doc, "name", queue_tags.get(i).name));
                queues.appendChild(queue);
                al_queue.add(queue);
            }

            Element places = doc.createElement("places");
            rootElement.appendChild(places);
            Element place;
            for (Iterator<QueuePlace> iterator = queue_places.iterator(); iterator.hasNext();) {
                QueuePlace queue_place = iterator.next();
                place = doc.createElement("place");
                place.setAttributeNode(createAttr(doc, "id", queue_place.id));
                place.setAttributeNode(createAttr(doc, "departure-discipline", "NORMAL"));
                place.setAttributeNode(createAttr(doc, "xsi:type", "queueing-place"));
                place.setAttributeNode(createAttr(doc, "name", queue_place.name));
                place.setAttributeNode(createAttr(doc, "queue-ref", queue_place.queue_ref));
                places.appendChild(place);
                // add meta-attributes tag
                Element meta_attributes = doc.createElement("meta-attributes");
                place.appendChild(meta_attributes);
                Element meta_attribute = doc.createElement("meta-attribute");
                meta_attribute.setAttributeNode(createAttr(doc, "xsi:type", "location-attribute"));
                meta_attribute.setAttributeNode(createAttr(doc, "location-x", "190"));
                meta_attribute.setAttributeNode(createAttr(doc, "location-y", "127"));
                meta_attributes.appendChild(meta_attribute);

                meta_attribute = doc.createElement("meta-attribute");
                meta_attribute.setAttributeNode(createAttr(doc, "xsi:type", "simqpn-place-configuration"));
                meta_attribute.setAttributeNode(createAttr(doc, "id", getId()));
                meta_attribute.setAttributeNode(createAttr(doc, "configuration-name", "sync-1")); //???
                meta_attribute.setAttributeNode(createAttr(doc, "statsLevel", "3"));
                meta_attributes.appendChild(meta_attribute);

                // add color-refs;
                Element color_refs = doc.createElement("color-refs");
                place.appendChild(color_refs);
                Element color_ref = doc.createElement("color-ref");
                color_refs.appendChild(color_ref);
                color_ref.setAttributeNode(createAttr(doc, "id", getId()));
                color_ref.setAttributeNode(createAttr(doc, "color-id", queue_place.lifeline_id)); // vi color_id = lifeline_id
                color_ref.setAttributeNode(createAttr(doc, "maximum-capacity", "0"));
                color_ref.setAttributeNode(createAttr(doc, "xsi:type", "queueing-color-reference"));
                color_ref.setAttributeNode(createAttr(doc, "ranking", "0"));
                color_ref.setAttributeNode(createAttr(doc, "priority", "0"));
                color_ref.setAttributeNode(createAttr(doc, "distribution-function", "Exponential"));
                if (queue_places.indexOf(queue_place) == 0) {
                    color_ref.setAttributeNode(createAttr(doc, "lambda", "0.01"));   //???
                    color_ref.setAttributeNode(createAttr(doc, "initial-population", "1"));  //???
                } else {
                    color_ref.setAttributeNode(createAttr(doc, "lambda", "1"));//???
                    color_ref.setAttributeNode(createAttr(doc, "initial-population", "0"));//???
                }
                // add meta-attributes of color-ref
                meta_attributes = doc.createElement("meta-attributes");
                color_ref.appendChild(meta_attributes);
                meta_attribute = doc.createElement("meta-attribute");
                meta_attributes.appendChild(meta_attribute);
                meta_attribute.setAttributeNode(createAttr(doc, "id", getId()));
                meta_attribute.setAttributeNode(createAttr(doc, "xsi:type", "simqpn-batch-means-queueing-color-configuration"));
                meta_attribute.setAttributeNode(createAttr(doc, "configuration-name", "sync-1"));//???
                meta_attribute.setAttributeNode(createAttr(doc, "signLev", "0.05"));
                meta_attribute.setAttributeNode(createAttr(doc, "reqAbsPrc", "50"));
                meta_attribute.setAttributeNode(createAttr(doc, "reqRelPrc", "0.05"));
                meta_attribute.setAttributeNode(createAttr(doc, "batchSize", "200"));
                meta_attribute.setAttributeNode(createAttr(doc, "minBatches", "60"));
                meta_attribute.setAttributeNode(createAttr(doc, "numBMeansCorlTested", "50"));
                meta_attribute.setAttributeNode(createAttr(doc, "bucketSize", "100.0"));
                meta_attribute.setAttributeNode(createAttr(doc, "maxBuckets", "1000"));
                meta_attribute.setAttributeNode(createAttr(doc, "queueSignLev", "0.05"));
                meta_attribute.setAttributeNode(createAttr(doc, "queueReqAbsPrc", "50"));
                meta_attribute.setAttributeNode(createAttr(doc, "queueBucketSize", "100.0"));
                meta_attribute.setAttributeNode(createAttr(doc, "queueMaxBuckets", "1000"));
                // check have message to --> co nhieu color-ref
                PapyrusMessageTag message_to;
                if ((message_to = checkHaveMessageTo(queue_place)) != null) {
                    color_ref = doc.createElement("color-ref");
                    color_refs.appendChild(color_ref);
                    color_ref.setAttributeNode(createAttr(doc, "id", getId()));
                    color_ref.setAttributeNode(createAttr(doc, "color-id", message_to.id)); // vi color_id = message_id
                    color_ref.setAttributeNode(createAttr(doc, "maximum-capacity", "0"));
                    color_ref.setAttributeNode(createAttr(doc, "xsi:type", "queueing-color-reference"));
                    color_ref.setAttributeNode(createAttr(doc, "ranking", "0"));
                    color_ref.setAttributeNode(createAttr(doc, "priority", "0"));
                    color_ref.setAttributeNode(createAttr(doc, "distribution-function", "Exponential"));
                    if (queue_places.indexOf(queue_place) == 0) {
                        color_ref.setAttributeNode(createAttr(doc, "lambda", "0.01"));   //???
                        color_ref.setAttributeNode(createAttr(doc, "initial-population", "1"));  //???
                    } else {
                        color_ref.setAttributeNode(createAttr(doc, "lambda", "1"));//???
                        color_ref.setAttributeNode(createAttr(doc, "initial-population", "0"));//???
                    }
                    // add meta-attributes of color-ref
                    meta_attributes = doc.createElement("meta-attributes");
                    color_ref.appendChild(meta_attributes);
                    meta_attribute = doc.createElement("meta-attribute");
                    meta_attributes.appendChild(meta_attribute);
                    meta_attribute.setAttributeNode(createAttr(doc, "id", getId()));
                    meta_attribute.setAttributeNode(createAttr(doc, "xsi:type", "simqpn-batch-means-queueing-color-configuration"));
                    meta_attribute.setAttributeNode(createAttr(doc, "configuration-name", "sync-1"));//???
                    meta_attribute.setAttributeNode(createAttr(doc, "signLev", "0.05"));
                    meta_attribute.setAttributeNode(createAttr(doc, "reqAbsPrc", "50"));
                    meta_attribute.setAttributeNode(createAttr(doc, "reqRelPrc", "0.05"));
                    meta_attribute.setAttributeNode(createAttr(doc, "batchSize", "200"));
                    meta_attribute.setAttributeNode(createAttr(doc, "minBatches", "60"));
                    meta_attribute.setAttributeNode(createAttr(doc, "numBMeansCorlTested", "50"));
                    meta_attribute.setAttributeNode(createAttr(doc, "bucketSize", "100.0"));
                    meta_attribute.setAttributeNode(createAttr(doc, "maxBuckets", "1000"));
                    meta_attribute.setAttributeNode(createAttr(doc, "queueSignLev", "0.05"));
                    meta_attribute.setAttributeNode(createAttr(doc, "queueReqAbsPrc", "50"));
                    meta_attribute.setAttributeNode(createAttr(doc, "queueBucketSize", "100.0"));
                    meta_attribute.setAttributeNode(createAttr(doc, "queueMaxBuckets", "1000"));
                }
            }

            // tim reply message --> de add ordinary-place sync
            QueuePlace queue_recv;
            QueuePlace queue_send;
            QueuePlace middle_queue;
            PapyrusMessageTag message_send;
            for (Iterator<PapyrusMessageTag> iterator = p_message_tags.iterator(); iterator.hasNext();) {
                PapyrusMessageTag message = iterator.next();
                if (isReplyMessage(message)) {
                    int index_queue_recv = getQueueIndexById(message.receiveEvent);
                    queue_recv = queue_places.get(index_queue_recv); // queue2
                    int index_queue_send = getQueueSendIndexFromQueueRecv(queue_recv);
                    if (index_queue_send == -1) {
                        continue;
                    }
                    queue_send = queue_places.get(index_queue_send); // queue1
                    middle_queue = queue_places.get(getQueueIndexById(message.sendEvent));
                    SynchOrdinaryPlace synchOrdinaryPlace = new SynchOrdinaryPlace(queue_send, queue_recv, middle_queue);

                    String name_place = "Sync" + p_lifeline_tags.get(getLifelineById(queue_send.lifeline_id)).name + (Integer.parseInt(queue_send.name.replaceAll("[\\D]", ""))) + (Integer.parseInt(queue_recv.name.replaceAll("[\\D]", "")));
                    message_send = findMessageSendFromQueue(queue_send);
                    if (message_send != null) {
                        place = doc.createElement("place");
                        place.setAttributeNode(createAttr(doc, "id", getId()));
                        synchOrdinaryPlace.place_id = place.getAttribute("id");
                        sync_ordinary_places.add(synchOrdinaryPlace);
                        place.setAttributeNode(createAttr(doc, "departure-discipline", "NORMAL"));
                        place.setAttributeNode(createAttr(doc, "xsi:type", "ordinary-place"));
                        place.setAttributeNode(createAttr(doc, "name", name_place));
                        places.appendChild(place);
                        // add meta-attributes tag
                        Element meta_attributes = doc.createElement("meta-attributes");
                        place.appendChild(meta_attributes);
                        Element meta_attribute = doc.createElement("meta-attribute");
                        meta_attribute.setAttributeNode(createAttr(doc, "xsi:type", "location-attribute"));
                        meta_attribute.setAttributeNode(createAttr(doc, "location-x", "190"));
                        meta_attribute.setAttributeNode(createAttr(doc, "location-y", "127"));
                        meta_attributes.appendChild(meta_attribute);

                        meta_attribute = doc.createElement("meta-attribute");
                        meta_attribute.setAttributeNode(createAttr(doc, "xsi:type", "simqpn-place-configuration"));
                        meta_attribute.setAttributeNode(createAttr(doc, "id", getId()));
                        meta_attribute.setAttributeNode(createAttr(doc, "configuration-name", "sync-1")); //???
                        meta_attribute.setAttributeNode(createAttr(doc, "statsLevel", "1"));
                        meta_attributes.appendChild(meta_attribute);

                        // add color-refs;
                        Element color_refs = doc.createElement("color-refs");
                        place.appendChild(color_refs);
                        Element color_ref = doc.createElement("color-ref");
                        color_refs.appendChild(color_ref);
                        color_ref.setAttributeNode(createAttr(doc, "id", getId()));
                        color_ref.setAttributeNode(createAttr(doc, "color-id", queue_send.lifeline_id));
                        color_ref.setAttributeNode(createAttr(doc, "maximum-capacity", "0"));
                        color_ref.setAttributeNode(createAttr(doc, "xsi:type", "ordinary-color-reference"));
                        color_ref.setAttributeNode(createAttr(doc, "initial-population", "0"));

                    }
                };
            }

            // add Transitions
            // duyet lifeline --> duyet queue (vi moi queue có 1 transition)
            Element transitions = doc.createElement("transitions");
            Element parent_connections = doc.createElement("connections"); // connection noi giua place <-> transition
            rootElement.appendChild(transitions);
            rootElement.appendChild(parent_connections);

            for (Iterator<PapyrusLifelineTag> iterator = p_lifeline_tags.iterator(); iterator.hasNext();) {
                PapyrusLifelineTag lifeline = iterator.next();
                for (i = 0; i < lifeline.queues.size(); i++) {
                    QueuePlace from_queue = lifeline.queues.get(i);
                    QueuePlace to_queue = null;
                    Element transition = doc.createElement("transition");
                    Element parent_p2t_connection = doc.createElement("connection");
                    parent_connections.appendChild(parent_p2t_connection);
                    transitions.appendChild(transition);
                    String transition_id = getId();
                    transition.setAttributeNode(createAttr(doc, "id", transition_id));
                    parent_p2t_connection.setAttributeNode(createAttr(doc, "target-id", transition_id));
                    transition.setAttributeNode(createAttr(doc, "priority", "0")); //???
                    transition.setAttributeNode(createAttr(doc, "xsi:type", "immediate-transition")); //???
                    transition.setAttributeNode(createAttr(doc, "weight", "1.0")); //???
                    transition.setAttributeNode(createAttr(doc, "name", "t" + from_queue.name));

                    Element meta_attributes = doc.createElement("meta-attributes");
                    transition.appendChild(meta_attributes);
                    Element meta_attribute = doc.createElement("meta-attribute");
                    meta_attributes.appendChild(meta_attribute);
                    meta_attribute.setAttributeNode(createAttr(doc, "xsi:type", "location-attribute"));
                    meta_attribute.setAttributeNode(createAttr(doc, "location-x", "259"));
                    meta_attribute.setAttributeNode(createAttr(doc, "location-y", "127"));

                    Element modes = doc.createElement("modes");
                    transition.appendChild(modes);
                    Element mode = doc.createElement("mode");
                    modes.appendChild(mode);
                    String mode_id = getId();
                    mode.setAttributeNode(createAttr(doc, "id", mode_id));
                    mode.setAttributeNode(createAttr(doc, "real-color", ColorToHex(randomColor())));
                    mode.setAttributeNode(createAttr(doc, "firing-weight", "1.0"));
                    mode.setAttributeNode(createAttr(doc, "name", "t" + from_queue.name));

                    Element connections = doc.createElement("connections");
                    transition.appendChild(connections);

                    Element place_element_of_from_queue = getPlaceElementFromId(doc, from_queue.id);
                    parent_p2t_connection.setAttributeNode(createAttr(doc, "source-id", place_element_of_from_queue.getAttribute("id")));
                    PapyrusMessageTag message_to = checkHaveMessageTo(from_queue);
                    NodeList color_refs_from = null;
                    color_refs_from = place_element_of_from_queue.getElementsByTagName("color-ref");
                    for (int j = 0; j < color_refs_from.getLength(); j++) {
                        Element color_ref = (Element) color_refs_from.item(j);
                        System.out.println(color_ref.getAttribute("id"));
                        Element connection = doc.createElement("connection");
                        connection.setAttributeNode(createAttr(doc, "id", getId()));
                        connection.setAttributeNode(createAttr(doc, "count", "1")); // number of token - defautl 1
                        connection.setAttributeNode(createAttr(doc, "source-id", color_ref.getAttribute("id")));
                        connection.setAttributeNode(createAttr(doc, "target-id", mode_id));
                        connections.appendChild(connection);

                        if ((i == 0 && color_ref.getAttribute("color-id").equals(lifeline.id))) {
                            // them connection tu transition -> place dau tien
                            Element parent_t2p_connection = doc.createElement("connection");
                            parent_connections.appendChild(parent_t2p_connection);
                            parent_t2p_connection.setAttributeNode(createAttr(doc, "source-id", transition_id));
                            parent_t2p_connection.setAttributeNode(createAttr(doc, "target-id", place_element_of_from_queue.getAttribute("id")));

                            // theo connection tu mode -> place dau tien
                            connection = doc.createElement("connection");
                            connection.setAttributeNode(createAttr(doc, "id", getId()));
                            connection.setAttributeNode(createAttr(doc, "count", "1")); // number of token - defautl 1
                            connection.setAttributeNode(createAttr(doc, "source-id", mode_id));
                            connection.setAttributeNode(createAttr(doc, "target-id", color_ref.getAttribute("id")));
                            connections.appendChild(connection);
                        }
                    }

                    // check xem co message nao tu queue nay khong?
                    PapyrusMessageTag message_from = findMessageSendFromQueue(from_queue);
                    if (message_from != null) {
                        // co message xuat phat tu queue nay --> tim queue den!
                        to_queue = queue_places.get(getQueueIndexById(message_from.receiveEvent));
                        Element parent_t2p_msg_connection = doc.createElement("connection");
                        parent_connections.appendChild(parent_t2p_msg_connection);
                        parent_t2p_msg_connection.setAttributeNode(createAttr(doc, "source-id", transition_id));
                        Element place_element_of_to_queue = getPlaceElementFromId(doc, to_queue.id);
                        parent_t2p_msg_connection.setAttributeNode(createAttr(doc, "target-id", place_element_of_to_queue.getAttribute("id")));
                        NodeList color_refs_to = place_element_of_to_queue.getElementsByTagName("color-ref");
                        boolean flag_get_only_color_of_message = true;
                        if (message_from.messageSort != null && message_from.messageSort.equals("reply")) {
                            flag_get_only_color_of_message = false; // flage check for only B2 ref two 2 color-ref of A3
                        }
                        for (int j = 0; j < color_refs_to.getLength(); j++) {
                            Element color_ref = (Element) color_refs_to.item(j);
                            Element connection = doc.createElement("connection");
                            connection.setAttributeNode(createAttr(doc, "id", getId()));
                            connection.setAttributeNode(createAttr(doc, "count", "1")); // number of token - defautl 1
                            connection.setAttributeNode(createAttr(doc, "source-id", mode_id));
                            connection.setAttributeNode(createAttr(doc, "target-id", color_ref.getAttribute("id")));
                            if (flag_get_only_color_of_message && color_ref.getAttribute("color-id").equals(message_from.id)) { // vi ban dau khoi tao color_id = message_i
                                connections.appendChild(connection);
                            } else if (flag_get_only_color_of_message == false) {
                                connections.appendChild(connection);
                            }
                        }
                    }
                    Element place_element_of_to_queue = null; // place of sync or normal -> from_queue
                    // check hava synch ordinary place --> queue (A2 -> syncA23)
                    SynchOrdinaryPlace synchOrdinaryPlace = checkHaveSynchOrdinaryPlaceFromQueue(from_queue);
                    if (synchOrdinaryPlace != null) {
                        place_element_of_to_queue = getPlaceElementFromId(doc, synchOrdinaryPlace.place_id);
                    } else {
                        to_queue = getNextQueueOfSameLifeline(from_queue);
                        if (to_queue != null) {
                            place_element_of_to_queue = getPlaceElementFromId(doc, to_queue.id);
                        }
                    }

                    if (place_element_of_to_queue != null) {
                        Element parent_t2p_sync_or_normal_connection = doc.createElement("connection");
                        parent_connections.appendChild(parent_t2p_sync_or_normal_connection);
                        parent_t2p_sync_or_normal_connection.setAttributeNode(createAttr(doc, "source-id", transition_id));
                        parent_t2p_sync_or_normal_connection.setAttributeNode(createAttr(doc, "target-id", place_element_of_to_queue.getAttribute("id")));

                        NodeList color_refs = place_element_of_to_queue.getElementsByTagName("color-ref");
                        NodeList color_refs_of_from_queue = place_element_of_from_queue.getElementsByTagName("color-ref");
                        
                        // only create connection mode -> color-ref (of to place) with: have color-id same with color-ref of from place
                        ArrayList<String> color_refs_ids_of_from_queue = new ArrayList<String>();
                        if (color_refs_of_from_queue != null) {
                            for (int j = 0; j < color_refs_of_from_queue.getLength(); j++) {
                                color_refs_ids_of_from_queue.add(((Element) color_refs_of_from_queue.item(j)).getAttribute("color-id"));
                            }
                        }
                        for (int j = 0; j < color_refs.getLength(); j++) {
                            Element color_ref = (Element) color_refs.item(j);
                            if (color_refs_ids_of_from_queue.indexOf(color_ref.getAttribute("color-id")) != -1) {
                                Element connection = doc.createElement("connection");
                                connection.setAttributeNode(createAttr(doc, "id", getId()));
                                connection.setAttributeNode(createAttr(doc, "count", "1")); // number of token - defautl 1
                                connection.setAttributeNode(createAttr(doc, "source-id", mode_id));
                                connection.setAttributeNode(createAttr(doc, "target-id", color_ref.getAttribute("id")));
                                connections.appendChild(connection);
                            }
                        }

                    }

                    // check hava synch ordinary place --> queue (syncA23->B2)
                    place_element_of_to_queue = null;
                    SynchOrdinaryPlace synchOrdinaryPlaceToMiddleQueue = checkHaveSynchOrdinaryPlaceToMiddleQueue(from_queue);
                    if (synchOrdinaryPlaceToMiddleQueue != null) {
                        place_element_of_to_queue = getPlaceElementFromId(doc, synchOrdinaryPlaceToMiddleQueue.place_id);
                    }

                    if (place_element_of_to_queue != null) {
                        Element parent_t2p_sync_or_normal_connection = doc.createElement("connection");
                        parent_connections.appendChild(parent_t2p_sync_or_normal_connection);
                        parent_t2p_sync_or_normal_connection.setAttributeNode(createAttr(doc, "source-id", place_element_of_to_queue.getAttribute("id")));
                        parent_t2p_sync_or_normal_connection.setAttributeNode(createAttr(doc, "target-id", transition_id));

                        NodeList color_refs = place_element_of_to_queue.getElementsByTagName("color-ref");
                        for (int j = 0; j < color_refs.getLength(); j++) {
                            Element color_ref = (Element) color_refs.item(j);
                            Element connection = doc.createElement("connection");
                            connection.setAttributeNode(createAttr(doc, "id", getId()));
                            connection.setAttributeNode(createAttr(doc, "count", "1")); // number of token - defautl 1
                            connection.setAttributeNode(createAttr(doc, "source-id", color_ref.getAttribute("id")));
                            connection.setAttributeNode(createAttr(doc, "target-id", mode_id));
                            connections.appendChild(connection);
                        }
                    }

                    if (i == (lifeline.queues.size() - 1)) {
                        Element parent_t2p_connection = doc.createElement("connection");
                        parent_connections.appendChild(parent_t2p_connection);
                        parent_t2p_connection.setAttributeNode(createAttr(doc, "source-id", transition_id));
                        parent_t2p_connection.setAttributeNode(createAttr(doc, "target-id", place_element_of_from_queue.getAttribute("id")));
                    }
                }

            }
            Element meta_attributes = doc.createElement("meta-attributes");
            rootElement.appendChild(meta_attributes);
            Element meta_attribute = doc.createElement("meta-attribute");
            meta_attributes.appendChild(meta_attribute);
            meta_attribute.setAttributeNode(createAttr(doc, "xsi:type", "simqpn-configuration"));
            meta_attribute.setAttributeNode(createAttr(doc, "id", getId()));
            meta_attribute.setAttributeNode(createAttr(doc, "scenario", "1"));
            meta_attribute.setAttributeNode(createAttr(doc, "stopping-rule", "FIXEDLEN"));
            meta_attribute.setAttributeNode(createAttr(doc, "time-before-initial-heart-beat", "100000"));
            meta_attribute.setAttributeNode(createAttr(doc, "time-between-stop-checks", "100000"));
            meta_attribute.setAttributeNode(createAttr(doc, "seconds-between-stop-checks", "60"));
            meta_attribute.setAttributeNode(createAttr(doc, "seconds-between-heart-beats", "60"));
            meta_attribute.setAttributeNode(createAttr(doc, "verbosity-level", "0"));
            meta_attribute.setAttributeNode(createAttr(doc, "output-directory", "."));
            meta_attribute.setAttributeNode(createAttr(doc, "configuration-name", "sync-1"));
            meta_attribute.setAttributeNode(createAttr(doc, "ramp-up-length", "1200.0"));
            meta_attribute.setAttributeNode(createAttr(doc, "total-run-length", "13200.0"));
            // write the content into xml file
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(new File("E:\\file.qpe"));

            // Output to console for testing
            // StreamResult result = new StreamResult(System.out);
            transformer.transform(source, result);

            System.out.println("File saved!");

        } catch (ParserConfigurationException pce) {
            pce.printStackTrace();
        } catch (TransformerException tfe) {
            tfe.printStackTrace();
        }
    }

}
