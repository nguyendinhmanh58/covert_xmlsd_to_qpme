/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package readxmlpapyrus;

import java.util.ArrayList;
import java.util.Iterator;
import org.w3c.dom.Element;

/**
 *
 * @author Nguyen Dinh Manh
 */
class PapyrusBaseTag {

    public String id;
    public String type;
    public String name;
}

class PapyrusLifelineTag extends PapyrusBaseTag {

    public String coveredBy;
    public int index_ocurrence = 1;
    public String color; // for qpme
    public String queue_id; // queue tuong ung voi lifelien for qpme
    public ArrayList<QueuePlace> queues = new ArrayList<QueuePlace>();
    public boolean containQueuePlace (QueuePlace queue_place) {
        for (Iterator<QueuePlace> iterator = queues.iterator(); iterator.hasNext();) {
            QueuePlace queue = iterator.next();
            if(queue == queue_place) return true;
        }
        return false;
    }
}

class PapyrusMessageTag extends PapyrusBaseTag {

    public String sendEvent;
    public String receiveEvent;
    public String messageSort;
    public String color; // for qpme
}

class PapyrusCombinedFragment extends PapyrusBaseTag {
    public String subnet_place_id;
    public ArrayList<PapyrusOperandTag> operands = new ArrayList<PapyrusOperandTag>();
    public ArrayList<PapyrusLifelineTag> covered_lifelines = new ArrayList<PapyrusLifelineTag>();
    public QueuePlace input_place = new QueuePlace();
    public QueuePlace output_place =  new QueuePlace();
    public String tALT_entry_id;
    public String tALT_exit_id;
    public String tALT_entry_mode_id;
    public String tALT_exit_mode_id;
    public Element tALT_entry_connections;
    public Element tALT_exit_connections;
    public ArrayList<String> color_ref_ids = new ArrayList<String>();
    
    public PapyrusOperandTag getOperandById(String id) {
        PapyrusOperandTag result = null;
        for (Iterator<PapyrusOperandTag> iterator = operands.iterator(); iterator.hasNext();) {
            PapyrusOperandTag operandTag = iterator.next();
            if (operandTag.id.equals(id)) {
                result = operandTag;
                break;
            }
        }
        return result;
    }
    
    public String getAltName() {
        String result="";
        for (Iterator<PapyrusLifelineTag> iterator = covered_lifelines.iterator(); iterator.hasNext();) {
            PapyrusLifelineTag lifeline = iterator.next();
            result+=lifeline.name;
        }
        return result;
    }
    
    public boolean containQueuePlace(QueuePlace place) {
        for (Iterator<PapyrusOperandTag> iterator = operands.iterator(); iterator.hasNext();) {
            PapyrusOperandTag operand = iterator.next();
            for (Iterator<QueuePlace> iterator1 = operand.queues.iterator(); iterator1.hasNext();) {
                QueuePlace queue_place = iterator1.next();
                if(queue_place == place) return true;
            }
        }
        return false;
    }
    
    // get first queue place of lifeline in ALT
    public QueuePlace getFirstQPOfLifeline (String lifeline_id) {
        for (Iterator<PapyrusOperandTag> iterator = operands.iterator(); iterator.hasNext();) {
            PapyrusOperandTag operand = iterator.next();
            for (Iterator<QueuePlace> iterator1 = operand.queues.iterator(); iterator1.hasNext();) {
                QueuePlace queue_place = iterator1.next();
                if(queue_place.lifeline_id.equals(lifeline_id)) return queue_place;
            }
        }
        return null;
    }
    
    // get last queue place of lifeline in ALT
    public QueuePlace getLastQPOfLifeline (String lifeline_id) {
        QueuePlace result = null;
        for (Iterator<PapyrusOperandTag> iterator = operands.iterator(); iterator.hasNext();) {
            PapyrusOperandTag operand = iterator.next();
            for (Iterator<QueuePlace> iterator1 = operand.queues.iterator(); iterator1.hasNext();) {
                QueuePlace queue_place = iterator1.next();
                if(queue_place.lifeline_id.equals(lifeline_id)) result = queue_place;
            }
        }
        return result;
    }
}

class PapyrusOperandTag extends PapyrusBaseTag {
    public String subnet_place_id;
    public ArrayList<QueuePlace> queues = new ArrayList<QueuePlace>();
    public QueuePlace input_place = new QueuePlace();
    public QueuePlace output_place =  new QueuePlace();
    public ArrayList<PapyrusLifelineTag> covered_lifelines = new ArrayList<PapyrusLifelineTag>();
    
    public ArrayList<QueuePlace> getQueueOfLifeLineInOperand(PapyrusLifelineTag lifelineTag) {
        ArrayList<QueuePlace> result = new ArrayList<QueuePlace>();
        for (Iterator<QueuePlace> iterator = queues.iterator(); iterator.hasNext();) {
            QueuePlace queue_place = iterator.next();
            if(lifelineTag.containQueuePlace(queue_place)) result.add(queue_place);
        }
        return result;
    }
}
